import { useState, useEffect } from 'react';

const usePlan = value => {
  const [plan, setPlan] = useState(null);

  useEffect(() => {
    setPlan(value);
  }, [value]);

  return { plan };
};

export default usePlan;
