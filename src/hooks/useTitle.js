import { useEffect } from 'react';

const useTitle = title => {
  useEffect(() => {
    document.title = `${title} | Autohound`;
  }, [title]);
};

export default useTitle;
