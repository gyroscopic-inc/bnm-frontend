import React from 'react';
import PropTypes from 'prop-types';

const OnePage = ({ children }) => <section className="min-h-screen bg-background">{children}</section>;

OnePage.propTypes = {
  children: PropTypes.object.isRequired,
  background: PropTypes.string,
};

export default OnePage;
