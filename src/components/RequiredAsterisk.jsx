import React from 'react';

const RequiredAsterisk = () => <p className="ml-1 inline text-sm text-error">*</p>;

export default RequiredAsterisk;
