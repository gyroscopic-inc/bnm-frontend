import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { getTokenPayload } from '../../utilities/token';
import {
  isCurrentUserACustomer,
  isCurrentUserAnAdmin,
  isCurrentUserAnOwner,
} from '../../utilities/authentication';

const LoggedinNavbar = ({ logout }) => {
  const { id, name } = getTokenPayload();

  let link = '';

  if (isCurrentUserAnAdmin()) link = '/admin/dashboard';
  if (isCurrentUserACustomer()) link = `/customer/${id}`;
  if (isCurrentUserAnOwner()) link = '/dashboard';

  return (
    <div className="px-20">
      <div className="flex items-center justify-center float-left h-full py-0">
        <Link
          to={link}
          className="mr-4 font-semibold bg-transparent text-primary text-h4 hover:text-opacity-80 active:opacity-80 disabled:bg-disabled"
        >
          Auto Hound
        </Link>
      </div>
      <div className="float-right py-0">
        <div className="grid">
          <p className="font-normal text-text bg-transparent text-body-2 hover:text-opacity-80 active:opacity-80 disabled:bg-disabled">
            {name}
          </p>
          <button
            className="font-normal text-right text-primary bg-opacity-80 text-small"
            onClick={() => logout()}
          >
            Logout
          </button>
        </div>
      </div>
    </div>
  );
};

LoggedinNavbar.propTypes = {
  logout: PropTypes.func,
  name: PropTypes.string,
};

export default LoggedinNavbar;
