import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import { deleteToken, isCurrentTokenValid } from '../../utilities/token';

import LandingNavbar from './LandingNavbar';
import SignupNavbar from './SignupNavbar';
import LoggedinNavbar from './LoggedinNavbar';

const Navbar = ({ location, history }) => {
  const isLoggedIn = isCurrentTokenValid();
  const isOnLandingPageAndIsNotLoggedIn = location.pathname === '/' && !isLoggedIn;
  const isOnUnAuthPage =
    ['/signup', '/signin', '/forgot-password', '/forgot-password/update-password'].includes(
      location.pathname
    ) && !isLoggedIn;

  const logout = () => {
    deleteToken();
    history.push('/');
  };

  const login = () => {
    history.push('/signin');
  };

  return (
    <div
      className={`${
        isOnLandingPageAndIsNotLoggedIn ? 'absolute border-0' : 'border-b border-gray-3 bg-background'
      } w-full pt-5`}
    >
      <div className="relative py-4 pb-16 mx-auto">
        {isOnLandingPageAndIsNotLoggedIn && <LandingNavbar />}
        {isOnUnAuthPage && <SignupNavbar login={login} />}
        {isLoggedIn && <LoggedinNavbar logout={logout} />}
      </div>
    </div>
  );
};

Navbar.propTypes = {
  location: PropTypes.object.isRequired,
  history: PropTypes.object,
};

export default withRouter(Navbar);
