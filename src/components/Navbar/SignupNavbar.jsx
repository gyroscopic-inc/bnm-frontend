import React from 'react';
import { Link } from 'react-router-dom';

const SignupNavbar = () => (
  <div className="container">
    <div className="flex items-center justify-center float-left h-full py-0">
      <Link
        to="/"
        className="mr-4 font-semibold bg-transparent text-text text-h4 hover:text-opacity-80 active:opacity-80 disabled:bg-disabled"
      >
        Auto Hound
      </Link>
    </div>
    <div className="float-right py-0">
      <>
        <p className="inline-block mr-6 font-normal text-text">Already a member?</p>
        <Link
          to="/signin"
          className="inline-block mr-4 font-semibold bg-transparent text-primary text-button hover:text-opacity-80 active:text-accent-dark disabled:bg-disabled"
        >
          Login
        </Link>
      </>
    </div>
  </div>
);

export default SignupNavbar;
