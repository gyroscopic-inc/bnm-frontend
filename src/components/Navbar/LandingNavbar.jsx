import React from 'react';
import { Link } from 'react-router-dom';

const LandingNavbar = () => (
  <div className="container ">
    <div className="flex items-center justify-center float-left h-full py-0">
      <Link
        to="/"
        className="mr-4 font-extrabold bg-transparent text-primary text-h6 hover:text-opacity-80 active:opacity-80 disabled:bg-disabled"
      >
        Auto Hound
      </Link>
    </div>
    <div className="float-right py-0">
      <>
        <Link
          to="/signin"
          className="mr-4 font-semibold text-white bg-transparent text-button hover:text-opacity-80 active:text-accent-dark disabled:bg-disabled"
        >
          Login
        </Link>
        <Link
          className="px-5 py-3 font-semibold text-white rounded-sm bg-primary text-Link hover:text-white hover:bg-opacity-80 active:bg-primary-light disabled:bg-disabled"
          to="/signup"
        >
          Signup
        </Link>
      </>
    </div>
  </div>
);

export default LandingNavbar;
