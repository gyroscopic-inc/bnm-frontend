import React from 'react';
import PropTypes from 'prop-types';

import RequiredAsterisk from './RequiredAsterisk';

const CurrencyInput = ({ label, subtext, name, value, onChange, required, ...rest }) => (
  <div className="flex flex-col w-full">
    <label className="inline text-small text-text">
      {label}
      {required === true && <RequiredAsterisk />}
    </label>
    {subtext?.length > 0 && <p className="italic text-text text-xs">{subtext}</p>}
    <div className="flex flex-row justify-start">
      <span className="bg-white text-base pl-4 pr-1 py-3 mt-1 mb-4 text-body-2 text-subtext">$</span>
      <input
        onChange={onChange}
        value={value}
        type="number"
        step="0.01"
        className="pr-4 py-3 mt-1 mb-4 text-body-2 text-subtext w-full focus:outline-none"
        name={name}
        placeholder="0.00"
        required={required}
        {...rest}
      />
    </div>
  </div>
);

CurrencyInput.propTypes = {
  label: PropTypes.string,
  subtext: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func,
  required: PropTypes.bool,
};

export default CurrencyInput;
