import React from 'react';
import PropTypes from 'prop-types';

const TextualButton = ({ bgColor = 'white', textColor = 'accent', font, size, children, disabled }) => {
  return (
    <button
      disabled={!!disabled}
      className={`bg-${bgColor} text-${textColor} font-${font} text-${size} mr-4 hover:text-accent-light active:text-accent-dark disabled:bg-disabled`}
    >
      {children}
    </button>
  );
};

TextualButton.propTypes = {
  bgColor: PropTypes.string,
  textColor: PropTypes.string,
  font: PropTypes.string,
  size: PropTypes.string,
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
};

export default TextualButton;
