import React from 'react';
import PropTypes from 'prop-types';

const ContainedButton = ({
  bgColor = 'primary',
  textColor = 'white',
  font,
  textSize,
  xPadding,
  children,
  disabled,
}) => (
  <button
    disabled={!!disabled}
    className={`bg-${bgColor} rounded-sm text-${textColor} px-5 py-3 font-${font} text-${textSize} px-${xPadding} rounded-sm hover:bg-accent-light active:bg-accent-dark disabled:bg-disabled`}
  >
    {children}
  </button>
);

ContainedButton.propTypes = {
  bgColor: PropTypes.string,
  textColor: PropTypes.string,
  font: PropTypes.string,
  textSize: PropTypes.string,
  xPadding: PropTypes.string,
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
};

export default ContainedButton;
