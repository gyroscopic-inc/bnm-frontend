import React from 'react';
import PropTypes from 'prop-types';

import RequiredAsterisk from './RequiredAsterisk';

const DateInput = ({ label, name, value, onChange, required, disabled }) => (
  <div className="flex flex-col w-full">
    <label className="inline text-small text-text">
      {label}
      {!!required && <RequiredAsterisk />}
    </label>
    <input
      onChange={onChange}
      value={value}
      type="date"
      className="px-4 py-3 mt-1 mb-4 bg-white text-body-2 text-subtext focus:outline-none"
      name={name}
      placeholder="(MM-DD-YYYY)"
      required={required}
      disabled={disabled}
    />
  </div>
);

DateInput.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
};

export default DateInput;
