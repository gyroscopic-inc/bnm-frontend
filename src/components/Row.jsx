import React from 'react';
import PropTypes from 'prop-types';

const Row = ({ children }) => <div className="w-full flex flex-row space-x-2">{children}</div>;

Row.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Row;
