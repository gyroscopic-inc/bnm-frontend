import React from 'react';
import PropTypes from 'prop-types';

import BackButton from './BackButton';

const FormHeader = ({ title, backLink }) => (
  <div className="flex flex-row items-center">
    <BackButton link={backLink} />
    <h3 className="text-h3 text-text font-bold mx-auto">{title}</h3>
  </div>
);

FormHeader.propTypes = {
  title: PropTypes.string,
  backLink: PropTypes.string,
};

export default FormHeader;
