import React from 'react';
import PropTypes from 'prop-types';

import RequiredAsterisk from './RequiredAsterisk';

const SelectInput = ({ label, name, value, onChange, required, defaultOption, children, ...rest }) => (
  <div className="flex flex-col w-full">
    <label className="inline text-small text-text">
      {label}
      {required === true && <RequiredAsterisk />}
    </label>
    <select
      name={name}
      className="px-4 py-3 mt-1 mb-4 bg-white text-body-2 text-subtext appearance-none focus:outline-none"
      value={value}
      onChange={onChange}
      required={required}
      {...rest}
    >
      <option value="">{defaultOption}</option>
      {children}
    </select>
  </div>
);

SelectInput.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool.isRequired,
  defaultOption: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default SelectInput;
