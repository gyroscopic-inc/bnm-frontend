import React from 'react';
import PropTypes from 'prop-types';

import RequiredAsterisk from './RequiredAsterisk';

const TextArea = ({ label, subtext, name, value, onChange, required }) => (
  <div className="flex flex-col w-full">
    <label className="inline text-small text-text">
      {label}
      {required === true && <RequiredAsterisk />}
    </label>
    {subtext?.length > 0 && <p className="italic text-text text-xs">{subtext}</p>}
    <textarea
      onChange={onChange}
      value={value}
      type="text"
      className="px-4 py-3 mt-1 mb-4 h-20 bg-white text-body-2 text-subtext focus:outline-none"
      name={name}
      placeholder="Placeholder"
      required={required}
    />
  </div>
);

TextArea.propTypes = {
  label: PropTypes.string,
  subtext: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func,
  required: PropTypes.bool,
};

export default TextArea;
