import React from 'react';
import PropTypes from 'prop-types';

const RadioInput = ({ label, name, value, onClick, defaultChecked }) => (
  <div className="mr-5 flex items-center">
    <input
      defaultChecked={defaultChecked}
      onClick={onClick}
      value={value}
      type="radio"
      name={name}
      className="mr-2 text-primary"
    />
    <label className="text-small text-text">{label}</label>
  </div>
);

RadioInput.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  onClick: PropTypes.func,
  defaultChecked: PropTypes.bool,
};

export default RadioInput;
