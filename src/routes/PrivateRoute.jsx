import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { isCurrentTokenValid } from '../utilities/token';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const hasLoggedInUser = isCurrentTokenValid();

  return (
    <Route
      {...rest}
      render={props => (hasLoggedInUser ? <Component {...props} /> : <Redirect to="/signin" />)}
    />
  );
};
PrivateRoute.propTypes = {
  component: PropTypes.func,
};

export default PrivateRoute;
