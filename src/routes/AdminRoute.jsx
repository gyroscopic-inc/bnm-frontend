import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { isCurrentTokenValid } from '../utilities/token';
import { isCurrentUserAnAdmin } from '../utilities/authentication';

const AdminRoute = ({ component: Component, ...rest }) => {
  const isValidUser = isCurrentTokenValid() && isCurrentUserAnAdmin();

  return (
    <Route {...rest} render={props => (isValidUser ? <Component {...props} /> : <Redirect to="/signin" />)} />
  );
};
AdminRoute.propTypes = {
  component: PropTypes.func,
};

export default AdminRoute;
