import React, { useState } from 'react';
import { Switch, Route } from 'react-router-dom';

import PrivateRoute from './routes/PrivateRoute';
import AdminRoute from './routes/AdminRoute';

import Profile from './containers/Customer/Profile';
import AdminDashboard from './containers/Admin/Dashboard';
import Dashboard from './containers/Dashboard/Dashboard';
import AddCustomer from './containers/Customer/AddCustomer';
import { AddInvoice, EditInvoice } from './containers/Customer/Invoice';
import EditCustomer from './containers/Customer/EditCustomer';
import CustomerProfile from './containers/Customer/Profile';
import AddVehicle from './containers/Vehicle/AddVehicle';
import EditVehicle from './containers/Vehicle/EditVehicle';

import Signup from './containers/Signup/Signup';
import Signin from './containers/Signin/Signin';
import LandingPage from './containers/LandingPage/LandingPage';
import Reports from './containers/Reports/Reports';
import NoMatch from './containers/NoMatch';

import ForgotPassword from './containers/ForgotPassword/ForgotPassword';
import VerifyPasscode from './containers/ForgotPassword/VerifyPasscode';
import EnterNewPassword from './containers/ForgotPassword/EnterNewPassword';

import Navbar from './components/Navbar/Navbar';

import AppContext from './AppContext';

const App = () => {
  const [plan, setPlan] = useState('');
  const [currentUser, setCurrentUser] = useState({});

  return (
    <AppContext.Provider value={{ plan, setPlan, currentUser, setCurrentUser }}>
      <Navbar />
      <Switch>
        <PrivateRoute exact path="/profile" component={Profile} />
        <AdminRoute exact path="/admin/dashboard" component={AdminDashboard} />
        <PrivateRoute exact path="/dashboard" component={Dashboard} />
        <PrivateRoute exact path="/customer/add" component={AddCustomer} />
        <PrivateRoute exact path="/customer/:id" component={CustomerProfile} />
        <PrivateRoute exact path="/customer/:id/update" component={EditCustomer} />
        <PrivateRoute exact path="/customer/:customerId/vehicle/add" component={AddVehicle} />
        <PrivateRoute exact path="/customer/:userId/invoice/add" component={AddInvoice} />
        <PrivateRoute exact path="/customer/:userId/invoice/:invoiceId/edit" component={EditInvoice} />
        <PrivateRoute exact path="/customer/:customerId/vehicle/:vehicleId/update" component={EditVehicle} />
        <PrivateRoute exact path="/reports/:id" component={Reports} />

        <Route exact path="/" component={LandingPage} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/signin" component={Signin} />
        <Route exact path="/forgot-password/" component={ForgotPassword} />
        <Route exact path="/forgot-password/verify/:mobile" component={VerifyPasscode} />
        <Route exact path="/forgot-password/update-password" component={EnterNewPassword} />
        <Route path="*">
          <NoMatch />
        </Route>
      </Switch>
    </AppContext.Provider>
  );
};

export default App;
