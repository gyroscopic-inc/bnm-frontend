import axios from 'axios';

import { getToken, isCurrentTokenValid } from '../utilities/token';

export default class HttpService {
  constructor(endpoint) {
    this.endpoint = endpoint;
    this.config = {
      headers: {
        Authorization: getToken(),
      },
    };
    this.url = `${process.env.REACT_APP_API_URL}${this.endpoint}`;
  }

  unsecuredGet() {
    return axios.get(this.url);
  }

  get() {
    if (isCurrentTokenValid()) {
      return axios.get(this.url, this.config);
    } else {
      return Promise.reject(new Error('token is expired'));
    }
  }

  put(body) {
    if (isCurrentTokenValid()) {
      return axios.put(this.url, body, this.config);
    } else {
      return Promise.reject(new Error('token is expired'));
    }
  }

  unsecuredPost(body) {
    return axios.post(this.url, body);
  }

  post(body) {
    return axios.post(this.url, body, this.config);
  }
}
