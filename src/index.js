import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';

console.log(process.env);
const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLISH);

const fonts = [
  {
    family: 'Mulish',
    src: 'url(https://fonts.gstatic.com/s/mulish/v1/1Ptyg83HX_SGhgqO0yLcmjzUAuWexZNR8aevHZ47LTdNwA.woff)',
    weight: 500,
  },
];

import './index.css';
import App from './App';

const history = createBrowserHistory();

ReactDOM.render(
  <Elements stripe={stripePromise} fonts={fonts}>
    <Router history={history}>
      <App />
    </Router>
  </Elements>,
  document.getElementById('root')
);
