import { getToken, getTokenPayload } from './token';

export function isAuthenticated() {
  const token = getToken();

  return !!token;
}

export function isCurrentUserACustomer() {
  const { role } = getTokenPayload();

  return role === 'customer';
}

export function isCurrentUserAnOwner() {
  const { role } = getTokenPayload();

  return role === 'owner';
}

export function isCurrentUserAnAdmin() {
  const { role } = getTokenPayload();

  return role === 'admin';
}
