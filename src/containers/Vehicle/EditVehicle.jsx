import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';

import VehicleAPIs from '../../api/vehicle';

import VehicleFields from './VehicleFields';

const EditVehicle = () => {
  const [vehicle, setVehicle] = useState({});
  const [error, setError] = useState('');

  const { customerId, vehicleId } = useParams();
  const history = useHistory();

  const LINK_TO_CUSTOMER_PROFILE = `/customer/${customerId}`;

  const updateVehicle = async vehicleUpdates => {
    try {
      await VehicleAPIs.updateVehicle(
        {
          driverName: vehicleUpdates.driverName,
          initialCost: vehicleUpdates.initialCost,
          vin: vehicleUpdates.vin,
          make: vehicleUpdates.make,
          model: vehicleUpdates.model,
          color: vehicleUpdates.color,
          body: vehicleUpdates.body,
          transmission: vehicleUpdates.transmission,
          engine: vehicleUpdates.engine,
          yearManufactured: vehicleUpdates.yearManufactured,
          purchaseYear: vehicleUpdates.purchaseYear,
          ac: vehicleUpdates.ac,
          active: vehicleUpdates.active,
          plateNumber: vehicleUpdates.plateNumber,
        },
        vehicleId
      );

      history.push(LINK_TO_CUSTOMER_PROFILE);
    } catch (e) {
      setError(e?.response?.data?.error);
    }
  };

  useEffect(() => {
    (async () => {
      try {
        const response = await VehicleAPIs.getVehicle(vehicleId);
        setVehicle(response?.data?.data?.vehicle);
      } catch (e) {
        setError(e?.response?.data?.error);
      }
    })();
  }, [vehicleId]);

  return (
    <>
      {!error && Object.entries(vehicle).length > 0 ? (
        <VehicleFields
          title="Edit Vehicle"
          operation="edit"
          submitHandler={updateVehicle}
          error={error}
          vehicle={vehicle}
        />
      ) : (
        <div className="py-10">
          <p className="text-base text-center text-error">{error}</p>
        </div>
      )}
    </>
  );
};

export default EditVehicle;
