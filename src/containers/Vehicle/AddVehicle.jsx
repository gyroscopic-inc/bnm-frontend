import React, { useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';

import VehicleAPIs from '../../api/vehicle';

import VehicleFields from './VehicleFields';

const AddVehicle = () => {
  const [error, setError] = useState(null);
  const { customerId } = useParams();
  const history = useHistory();

  const addVehicle = async body => {
    try {
      await VehicleAPIs.addVehicle(body, customerId);
      history.push(`/customer/${customerId}`);
    } catch (error) {
      const { details } = error.response.data.error;

      if (details) {
        const { message } = error.response.data.error.details[0];
        setError(message);
      } else {
        setError(error?.response?.data?.error);
      }

      console.error(error.message);
    }
  };

  return (
    <VehicleFields title="Add A Vehicle" operation="add" submitHandler={addVehicle} parentError={error} />
  );
};

export default AddVehicle;
