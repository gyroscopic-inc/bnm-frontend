import React, { useState, useCallback, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import nhtsa from 'nhtsa';

import TextInput from '../../components/TextInput';
import CurrencyInput from '../../components/CurrencyInput';
import OnePage from '../../components/Pages/OnePage';
import Row from '../../components/Row';
import FormHeader from '../../components/FormHeader';

const VehicleFields = ({ title, operation = 'add', submitHandler, vehicle, parentError }) => {
  const [error, setError] = useState(null);
  const vehicleInitialState = {
    driverName: vehicle?.driverName ?? '',
    plateNumber: vehicle?.plateNumber ?? '',
    initialCost: vehicle?.initialCost ?? 0,
    vin: vehicle?.vin ?? '',
    make: vehicle?.make ?? '',
    model: vehicle?.model ?? '',
    color: vehicle?.color ?? '',
    body: vehicle?.body ?? '',
    transmission: vehicle?.transmission ?? '',
    engine: vehicle?.engine ?? '',
    yearManufactured: vehicle?.yearManufactured ?? '',
    purchaseYear: vehicle?.purchaseYear ?? '',
    ac: vehicle?.ac ?? true,
    active: vehicle?.active ?? true,
  };
  const [vehicleData, setVehicleData] = useState(vehicleInitialState);

  const { customerId } = useParams();

  const updateState = e => {
    const key = e.target.name;
    const value = e.target.value;

    setVehicleData({ ...vehicleData, [key]: value });
  };

  const addVehicle = e => {
    e.preventDefault();
    submitHandler(vehicleData);
  };

  const findVinAndUpdateVehicleDataState = useCallback(async (vin, vehicleInfo) => {
    try {
      const { data } = await nhtsa.decodeVinExtendedFlatFormat(vin);

      if (data) {
        const { Make, Model, BodyClass, TransmissionStyle, DisplacementL, ModelYear } = data.Results[0];
        setVehicleData({
          ...vehicleInfo,
          make: Make,
          model: Model,
          body: BodyClass,
          transmission: TransmissionStyle,
          engine: DisplacementL,
          yearManufactured: ModelYear,
        });
      }

      if (data.Results[0].ErrorText) {
        if (data.Results[0].ErrorText.includes('Manufacturer is not registered with NHTSA')) {
          setError(
            'The VIN belongs to a vehicle that is not registered with NHTSA. Unfortunately, details about the vehicle cannot be found'
          );
        }
      }
    } catch (error) {
      setError({ ...error });
    }
  }, []);

  useEffect(() => {
    const isAddOperation = operation === 'add';
    const isValidVin = vehicleData?.vin !== '' && vehicleData?.vin?.length === 17;
    const hasNoResultsYet = vehicleData?.make?.length === 0;

    if (isAddOperation && isValidVin && hasNoResultsYet && !error) {
      findVinAndUpdateVehicleDataState(vehicleData.vin, vehicleData);
    }
  }, [vehicleData.vin, operation, vehicleData, findVinAndUpdateVehicleDataState, error]);

  return (
    <OnePage background="background">
      <div className="container px-2 pt-10 pb-40 w-2/7 h-hero">
        <div className="flex flex-col h-auto px-2 pt-10 mt-20 w-100">
          <FormHeader title={title} backLink={`/customer/${customerId}`} />
          <form className="flex flex-col px-5 pt-10" onSubmit={e => addVehicle(e)}>
            <TextInput
              label="V.I.N"
              name="vin"
              value={vehicleData.vin}
              onChange={e => updateState(e)}
              required={true}
            />
            <TextInput
              label="Year Manufactured"
              name="yearManufactured"
              value={vehicleData.yearManufactured}
              onChange={e => updateState(e)}
              required={true}
            />
            <TextInput
              label="Plate Number"
              name="plateNumber"
              value={vehicleData.plateNumber}
              onChange={e => updateState(e)}
              required={true}
            />
            <Row>
              <TextInput
                label="Make"
                name="make"
                value={vehicleData.make}
                onChange={e => updateState(e)}
                required={true}
              />
              <TextInput
                label="Model"
                name="model"
                value={vehicleData.model}
                onChange={e => updateState(e)}
                required={true}
              />
            </Row>
            <TextInput
              label="Engine"
              name="engine"
              value={vehicleData.engine}
              onChange={e => updateState(e)}
              required={true}
            />
            <Row>
              <TextInput
                label="Driver Name"
                name="driverName"
                value={vehicleData.driverName}
                onChange={e => updateState(e)}
                required={false}
              />
            </Row>
            <CurrencyInput
              label="Initial Cost"
              name="initialCost"
              value={vehicleData.initialCost}
              onChange={e => updateState(e)}
              required={false}
            />
            {vehicleData?.initialCost > 0 && (
              <TextInput
                label="Purchase Year"
                subtext="Since an initial cost was provided, please enter the year it was purchased."
                name="purchaseYear"
                value={vehicleData.purchaseYear}
                onChange={e => updateState(e)}
                required={vehicleData?.initialCost > 0}
              />
            )}
            <Row>
              <TextInput
                label="Color"
                name="color"
                value={vehicleData.color}
                onChange={e => updateState(e)}
                required={false}
              />
              <TextInput
                label="Body"
                name="body"
                value={vehicleData.body}
                onChange={e => updateState(e)}
                required={false}
              />
            </Row>
            <Row className="flex flex-row w-full space-x-2">
              <TextInput
                label="Transmission"
                name="transmission"
                value={vehicleData.transmission}
                onChange={e => updateState(e)}
                required={false}
              />
            </Row>
            <div className="flex flex-col w-full">
              <label className="text-small text-text">Status</label>
              <div className="flex flex-row mt-3 mb-10">
                <div className="flex items-center mr-5">
                  <input
                    defaultChecked={vehicleData.active === true}
                    onChange={e => updateState(e)}
                    value="true"
                    type="radio"
                    name="active"
                    className="mr-2 text-primary"
                    required={false}
                  />
                  <label className="text-small text-text">Active</label>
                </div>
                <div className="flex items-center mr-5">
                  <input
                    defaultChecked={vehicleData.active === false}
                    onChange={e => updateState(e)}
                    value="false"
                    type="radio"
                    name="active"
                    className="mr-2 text-primary"
                    required={false}
                  />
                  <label className="text-small text-text">Inactive</label>
                </div>
              </div>
            </div>
            <div className="flex flex-col w-full">
              <label className="text-small text-text">A/C</label>
              <div className="flex flex-row mt-3 mb-10">
                <div className="flex items-center mr-5">
                  <input
                    defaultChecked={vehicleData.ac === true}
                    onChange={e => updateState(e)}
                    value="true"
                    type="radio"
                    name="ac"
                    className="mr-2 text-primary"
                    required={false}
                  />
                  <label className="text-small text-text">Yes</label>
                </div>
                <div className="flex items-center mr-5">
                  <input
                    defaultChecked={vehicleData.ac === false}
                    onChange={e => updateState(e)}
                    value="false"
                    type="radio"
                    name="ac"
                    className="mr-2 text-primary"
                    required={false}
                  />
                  <label className="text-small text-text">No</label>
                </div>
              </div>
            </div>
            <button
              type="submit"
              className="px-5 py-3 mt-5 text-white rounded-sm w-36 text-button bg-primary hover:bg-opacity-80"
            >
              {operation === 'add' ? 'Add Vehicle' : 'Save Changes'}
            </button>
          </form>
          {error?.length && (
            <div className="py-10">
              <p className="text-xs text-center text-error">{error}</p>
            </div>
          )}
          {parentError?.length && (
            <div className="py-10">
              <p className="text-xs text-center text-error">{parentError}</p>
            </div>
          )}
        </div>
      </div>
    </OnePage>
  );
};

VehicleFields.propTypes = {
  title: PropTypes.string,
  operation: PropTypes.string,
  submitHandler: PropTypes.func,
  vehicle: PropTypes.object,
  parentError: PropTypes.string,
};

export default VehicleFields;
