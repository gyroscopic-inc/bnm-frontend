import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { BasicError } from '../../components/Error';

const CustomerTable = ({ customers, sortByField }) => (
  <>
    {customers?.length > 0 ? (
      <table className="table-auto mt-5">
        <thead>
          <tr className="text-left bg-secondary p-5 text-text text-subtitle-2 font-medium">
            <th onClick={() => sortByField('name')} className="p-5 cursor-pointer">
              Name
            </th>
            <th onClick={() => sortByField('mobile')} className="p-5 cursor-pointer">
              Phone Number
            </th>
            <th onClick={() => sortByField('email')} className="p-5 cursor-pointer">
              Email Address
            </th>
            <th onClick={() => sortByField('status')} className="p-5 text-right cursor-pointer">
              Status
            </th>
            <th className="p-5"></th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer, i) => (
            <tr key={i} className="bg-white border-b border-secondary">
              <td className="p-5 text-gray-1 font-normal text-body-2">
                {customer.userId.firstName} {customer.userId.lastName}
              </td>
              <td className="p-5  text-gray-1 font-normal text-body-2">{customer.userId.mobile}</td>
              <td className="p-5  text-gray-1 font-normal text-body-2">{customer.userId.email}</td>
              <td className="p-5  text-gray-1 font-normal text-body-2 text-right">
                {customer.status ? 'Active' : 'Inactive'}
              </td>
              <td className="p-5  text-gray-1 font-normal text-body-2 text-right">
                <Link to={`/customer/${customer.userId._id}`} className="text-primary underline">
                  View Details
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    ) : (
      <BasicError customError="No customers found" />
    )}
  </>
);

CustomerTable.propTypes = {
  customers: PropTypes.array,
  sortByField: PropTypes.func,
};

export default CustomerTable;
