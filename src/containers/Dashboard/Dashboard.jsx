import React, { useState, useEffect, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { FiPlus, FiSearch, FiEdit2 } from 'react-icons/fi';

import UserAPIs from '../../api/user';
import AdminAPIs from '../../api/admin';

import OnePage from '../../components/Pages/OnePage';
import CustomerTable from './CustomerTable';

import useTitle from '../../hooks/useTitle';

const Dashboard = () => {
  const [error, setError] = useState({});
  const [sortField, setSortField] = useState(null);
  const [table, setTable] = useState([]);
  const [sortedTable, setSortedTable] = useState(null);
  const [garage, setGarage] = useState('');
  const [isEditingGarage, setIsEditingGarage] = useState(false);
  const [garageName, setGarageName] = useState('');
  const [garageId, setGarageId] = useState('');

  useTitle('Dashboard');

  const compareValues = useCallback(
    (key, order = 'asc') => (a, b) => {
      const isInStringColumn = key === 'name' || key === 'lastName' || key === 'email';
      const isCurrentlySortingByName = key === 'name';
      const isNumberColumn = key === 'mobile';

      let varA = a.userId[key];
      let varB = b.userId[key];

      if (isInStringColumn) {
        if (!isCurrentlySortingByName) {
          varA = varA.toUpperCase();
          varB = varB.toUpperCase();
        } else {
          varA = combineFirstAndLastNames(a);
          varB = combineFirstAndLastNames(b);
        }
      } else if (isNumberColumn) {
        varA = a.userId[key].replace(/\D/g, '');
        varB = b.userId[key].replace(/\D/g, '');
      }

      let comparison = 0;

      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }

      return order === 'desc' ? comparison * -1 : comparison;
    },
    []
  );

  const filterSortedTable = e => {
    const query = e.target.value;
    const sorted = getCustomersWithMatchingSubstrings(query);

    setSortedTable(sorted);
  };

  const getCustomersWithMatchingSubstrings = substring => {
    return table.filter(data => combineFirstAndLastNames(data).includes(substring.toUpperCase()));
  };

  const combineFirstAndLastNames = user => {
    return `${user.userId['firstName'].toUpperCase()} ${user.userId['lastName'].toUpperCase()}`;
  };

  const updateGarageName = async () => {
    try {
      setIsEditingGarage(false);
      const response = await AdminAPIs.updateGarageName(garageId, garageName);
      setGarage(response.data.garage.name);
    } catch (e) {
      setError(e.message);
    }
  };

  useEffect(() => {
    (async () => {
      try {
        const response = await UserAPIs.getCustomers();
        const { customers, name, _id } = response.data.garage;

        setGarage(name);
        setSortedTable(customers);
        setTable(customers);
        setGarageId(_id);
      } catch (e) {
        setError({ ...e });
      }
    })();
  }, []);

  useEffect(() => {
    if (sortedTable?.length > 0) {
      setSortedTable(sortedTable => [...sortedTable].sort(compareValues(sortField)));
    }
  }, [compareValues, sortField, sortedTable?.length]);

  return (
    <OnePage background="background">
      {sortedTable ? (
        <div className="px-20 flex flex-col relative pt-52">
          <div className="flex flex-row mb-5">
            <h1 className="text-base text-text font-regular">Garage Name: {garage}</h1>
            <button className="pl-2" onClick={() => setIsEditingGarage(!isEditingGarage)}>
              <FiEdit2 className="text-sm" />
            </button>
          </div>
          {isEditingGarage && (
            <div className="flex flex-row">
              <input
                type="text"
                placeholder="Enter a Garage Name"
                className="w-60 p-2 text-gray-1 font-normal rounded-r-none"
                onChange={e => setGarageName(e.target.value)}
                value={garageName}
              />
              <button
                className="bg-primary p-2 text-white text-sm rounded-r-sm"
                onClick={() => updateGarageName()}
              >
                Update Garage Name
              </button>
            </div>
          )}
          <h1 className="text-h5 text-text font-semibold mt-5">Customers</h1>
          <div>
            <div className="float-left mt-4 flex flex-row">
              <input
                onChange={e => filterSortedTable(e)}
                name="customer"
                placeholder="Find customer"
                className="py-2 px-4 text-gray-1 font-normal rounded-r-none"
              />
              <button className="bg-primary p-3 text-white rounded-r-sm">
                <FiSearch className="text-sm" />
              </button>
            </div>
            <div className="float-right">
              <button className="bg-primary hover:bg-opacity-80 py-2 px-4 text-white rounded-sm ">
                <Link to="/customer/add">
                  <FiPlus className="inline-block align-middle mr-2 text-base" />
                  <p className="inline-block align-middle">Add Customer</p>
                </Link>
              </button>
            </div>
          </div>
          {!!sortedTable && <CustomerTable customers={sortedTable} sortByField={setSortField} />}
        </div>
      ) : (
        <h1>{error.message}</h1>
      )}
    </OnePage>
  );
};

export default Dashboard;
