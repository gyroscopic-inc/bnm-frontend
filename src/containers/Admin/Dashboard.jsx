import React, { useEffect, useState } from 'react';

import AdminAPIs from '../../api/admin';

import OnePage from '../../components/Pages/OnePage';
import { PageError } from '../../components/Error';
import CustomerTable from '../../containers/Dashboard/CustomerTable';

import useTitle from '../../hooks/useTitle';

const AdminDashboard = () => {
  const [error, setError] = useState(null);
  const [customers, setCustomers] = useState([]);

  useTitle('Admin Dashboard');

  useEffect(() => {
    (async () => {
      try {
        const response = await AdminAPIs.getAllCustomers();
        setCustomers(response.data.customers);
      } catch (error) {
        setError(error);
      }
    })();
  }, []);

  return (
    <OnePage background="background">
      {customers.length > 0 && (
        <div className="px-20 flex flex-col relative pt-52">
          <h1 className="text-h5 text-text font-semibold mt-5 text-center">Autohound Customers</h1>
          <CustomerTable customers={customers} />
        </div>
      )}
      {!!error && <PageError message="No customers found." />}
    </OnePage>
  );
};

export default AdminDashboard;
