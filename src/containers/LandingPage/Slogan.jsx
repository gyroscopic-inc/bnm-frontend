import React from 'react';

const Slogan = () => {
  const mainSlogan = `Now man has a second best friend!`;
  const subSlogan = `Request your trustworthy auto shop to better serve you and become an Auto Hound affiliate.`;

  return (
    <div className="bg-cover bg-hero">
      <div className="container px-8 pb-40 pt-32">
        <div className="w-3/4 py-24">
          <h2 className="font-bold text-white text-h2">{mainSlogan}</h2>
          <p className="w-4/5 my-10 font-normal text-white text-subtitle-1">{subSlogan}</p>
          <a
            href="#plans"
            className="py-3 font-semibold text-white rounded-sm text-button bg-primary px-11 hover:bg-opacity-80 disabled:bg-disabled"
          >
            Choose a plan
          </a>
        </div>
      </div>
    </div>
  );
};

export default Slogan;
