import React from 'react';

import Slogan from './Slogan';
import ServicesSection from './ServicesSection';
import PlansSection from './PlansSection';

import useTitle from '../../hooks/useTitle';

const LandingPage = () => {
  useTitle('Welcome!');

  return (
    <section>
      <Slogan />
      <ServicesSection />
      <PlansSection />
    </section>
  );
};

export default LandingPage;
