import React from 'react';
import { useLocation } from 'react-router-dom';

import OnePage from '../components/Pages/OnePage';

const NoMatch = () => {
  const location = useLocation();

  return (
    <OnePage>
      <div>No match for {location.pathname}</div>
    </OnePage>
  );
};

export default NoMatch;
