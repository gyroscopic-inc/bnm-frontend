import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';
import PropTypes from 'prop-types';

import OnePage from '../../components/Pages/OnePage';

import { isCurrentUserACustomer } from '../../utilities/authentication';

import TextInput from '../../components/TextInput';
import CurrencyInput from '../../components/CurrencyInput';
import Row from '../../components/Row';
import TextArea from '../../components/TextArea';
import RadioInput from '../../components/RadioInput';

const CustomerFields = ({ title, operation = 'add', submitHandler, data, error }) => {
  const [customer, setCustomerData] = useState({
    firstName: '',
    lastName: '',
    mobile: '',
    email: '',
    ...data,
    customerId: {
      city: '',
      province: '',
      street: '',
      postalCode: '',
      labourRate: '',
      notes: '',
      status: false,
      discount: '',
      ...data.customerId,
    },
  });
  const customerIdFields = [
    'province',
    'postalCode',
    'labourRate',
    'street',
    'city',
    'notes',
    'status',
    'discount',
  ];

  const updateState = e => {
    const key = e.target.name;
    let value = e.target.value;
    const isCustomerIdField = customerIdFields.includes(key);

    if (isCustomerIdField) {
      if (key === 'status') {
        value = value === 'true'; // converts the string into a Boolean value
      }

      setCustomerData({ ...customer, customerId: { ...customer.customerId, [key]: value } });
    } else {
      setCustomerData({ ...customer, [key]: value });
    }
  };

  // const resetForm = () => {
  //   setCustomerData({
  //     firstName: '',
  //     lastName: '',
  //     mobile: '',
  //     email: '',
  //     ...data,
  //     customerId: {
  //       city: '',
  //       province: '',
  //       street: '',
  //       postalCode: '',
  //       labourRate: '',
  //       notes: '',
  //       status: false,
  //       discount: '',
  //       ...data.customerId,
  //     },
  //   });
  // };

  const handleSubmit = e => {
    e.preventDefault();
    submitHandler(customer);
  };

  return (
    <OnePage background="background">
      <div className="container w-2/7 px-2 pt-10 pb-40 h-hero">
        <div className="flex flex-col h-auto px-2 pt-10 w-100 mt-20">
          <div className="flex flex-row items-center">
            <Link to={`${operation === 'add' ? '/dashboard' : `/customer/${data._id}`}`}>
              <FiArrowLeft className="text-3xl text-text" />
            </Link>
            <h3 className="text-h3 text-text font-bold mx-auto">{title}</h3>
          </div>
          <form className="flex flex-col pt-10 px-5" onSubmit={e => handleSubmit(e)}>
            <Row>
              <TextInput
                label="First Name"
                onChange={e => updateState(e)}
                name="firstName"
                value={customer?.firstName}
                required={true}
                placeholder="John"
              />
              <TextInput
                label="Last Name"
                onChange={e => updateState(e)}
                name="lastName"
                value={customer?.lastName}
                required={true}
                placeholder="Doe"
              />
            </Row>
            <Row>
              <TextInput
                label="Mobile"
                onChange={e => updateState(e)}
                name="mobile"
                value={customer?.mobile}
                required={true}
                placeholder="123-456-7890"
              />
              <TextInput
                label="Email"
                onChange={e => updateState(e)}
                name="email"
                value={customer?.email}
                required={true}
                placeholder="john.doe@mail.com"
              />
            </Row>
            <Row>
              <TextInput
                label="City"
                onChange={e => updateState(e)}
                name="city"
                value={customer?.customerId?.city}
                required={false}
                placeholder="Toronto"
              />
              <TextInput
                label="Province"
                onChange={e => updateState(e)}
                name="province"
                value={customer?.customerId?.province}
                required={false}
                placeholder="Ontario"
              />
            </Row>
            <Row>
              <TextInput
                label="Street"
                onChange={e => updateState(e)}
                name="street"
                value={customer?.customerId?.street}
                required={false}
                placeholder="e.g 100 Bayview Ave"
              />
              <TextInput
                label="Postal Code"
                onChange={e => updateState(e)}
                name="postalCode"
                value={customer?.customerId?.postalCode}
                required={false}
                placeholder="e.g M1M 1M1"
              />
            </Row>
            {!isCurrentUserACustomer() && (
              <>
                <Row>
                  <CurrencyInput
                    label="Labour Rate / Hour"
                    onChange={e => updateState(e)}
                    name="labourRate"
                    value={customer?.customerId?.labourRate}
                    required={true}
                    placeholder="0.00"
                  />
                </Row>
                <Row>
                  <CurrencyInput
                    label="Discount"
                    onChange={e => updateState(e)}
                    name="discount"
                    value={customer?.customerId?.discount}
                    required={false}
                    placeholder="0.00"
                  />
                </Row>
                <Row>
                  <TextArea
                    label="Notes"
                    onChange={e => updateState(e)}
                    name="notes"
                    value={customer?.customerId?.notes}
                    required={false}
                    placeholder="Enter relevant notes"
                  />
                </Row>
                {operation === 'edit' && (
                  <>
                    <label className="text-small text-text">Status</label>
                    <div className="flex flex-row mt-3 mb-10">
                      <RadioInput
                        label="Active"
                        defaultChecked={customer?.customerId?.status === true}
                        onClick={e => updateState(e)}
                        value="true"
                        name="status"
                      />
                      <RadioInput
                        label="Inactive"
                        defaultChecked={customer?.customerId?.status === false}
                        onClick={e => updateState(e)}
                        value="false"
                        name="status"
                      />
                    </div>
                  </>
                )}
              </>
            )}
            <button
              type="submit"
              className="px-5 py-3 mt-5 w-36 text-white text-button rounded-sm bg-primary hover:bg-opacity-80"
            >
              {operation === 'add' ? 'Add Customer' : 'Save Changes'}
            </button>
          </form>
          {error && (
            <div className="my-10">
              <p className="text-sm text-center text-error">{error}</p>
            </div>
          )}
        </div>
      </div>
    </OnePage>
  );
};

CustomerFields.propTypes = {
  title: PropTypes.string,
  operation: PropTypes.string,
  submitHandler: PropTypes.func,
  data: PropTypes.object,
  error: PropTypes.string,
};

export default CustomerFields;
