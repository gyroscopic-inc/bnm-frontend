import React, { useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';

import UserAPIs from '../../api/user';

import OnePage from '../../components/Pages/OnePage';
import { PageError } from '../../components/Error';

import BasicInfo from './BasicInfo';
import Vehicles from './Vehicles';
import { Invoices } from './Invoice';

import useTitle from '../../hooks/useTitle';

const CustomerProfile = () => {
  const [error, setError] = useState({});
  const [userData, setUserData] = useState({});
  const [separatedInvoices, setSeparatedInvoices] = useState({});

  useTitle('Profile');

  const { id } = useParams();

  useEffect(() => {
    (async () => {
      try {
        const response = await UserAPIs.getCustomer(id);
        const { user } = response.data;

        setUserData({ ...user });
      } catch (e) {
        setError({ ...e });
      }
    })();
  }, [id]);

  const filterVehiclesWithInvoices = useCallback(vehicles => {
    const vehiclesWithInvoices = vehicles.filter(vehicle => vehicle?.invoices?.length !== 0);

    vehiclesWithInvoices.forEach(vehicleWithInvoice => {
      vehicleWithInvoice?.invoices.forEach(invoice =>
        createVehiclePropertyWithinInvoice(vehicleWithInvoice, invoice)
      );
    });

    const totalInvoices = vehiclesWithInvoices.map(vehicle => vehicle?.invoices).flat();
    const years = [...new Set(totalInvoices.map(invoice => new Date(invoice?.date).getUTCFullYear()))];

    const obj = {};

    years.forEach(year => {
      const invoicesInCurrentYear = totalInvoices.filter(
        invoice => new Date(invoice.date).getUTCFullYear() === year
      );

      obj[year] = [...invoicesInCurrentYear];
    });

    setSeparatedInvoices({ ...obj });
  }, []);

  const createVehiclePropertyWithinInvoice = ({ make, model }, invoice) => {
    invoice.vehicle = `${make} ${model}`;
  };

  useEffect(() => {
    const isValidUserData = Object.entries(userData)?.length > 0 && userData.constructor === Object;

    if (isValidUserData) {
      const hasVehicles = userData?.customerId?.vehicles?.length > 0;

      if (hasVehicles) {
        filterVehiclesWithInvoices(userData.customerId.vehicles);
      }
    }
  }, [filterVehiclesWithInvoices, userData]);

  return (
    <>
      <OnePage>
        {Object.entries(error).length > 0 && (
          <PageError message={'An error occurred while processing your request.'} />
        )}
        {userData.customerId && (
          <div className="h-full">
            <div className="flex flex-row h-auto min-h-screen bg-background">
              <BasicInfo user={userData} />
              <div className="w-3/4">
                <Vehicles vehicles={userData.customerId.vehicles} id={id} />
                <Invoices invoices={separatedInvoices} />
              </div>
            </div>
          </div>
        )}
      </OnePage>
    </>
  );
};

export default CustomerProfile;
