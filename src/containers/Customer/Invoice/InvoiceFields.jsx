import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';

import OnePage from '../../../components/Pages/OnePage';
import TextInput from '../../../components/TextInput';
import NumberInput from '../../../components/NumberInput';
import CurrencyInput from '../../../components/CurrencyInput';
import DateInput from '../../../components/DateInput';
import FormHeader from '../../../components/FormHeader';
import Row from '../../../components/Row';
import SelectInput from '../../../components/SelectInput';
import TextArea from '../../../components/TextArea';
import { BasicError } from '../../../components/Error';

import { isCurrentUserAnAdmin, isCurrentUserAnOwner } from '../../../utilities/authentication';

const InvoiceFields = ({ cars, operation, invoice, handler, title, oilChange }) => {
  const initialInvoiceState = {
    oilChange: oilChange ?? 0,
    date: invoice?.date ?? '',
    number: invoice?.number ?? '',
    mileage: invoice?.mileage ?? '',
    type: invoice?.type ?? '',
    repairDescription: invoice?.repairDescription ?? '',
    totalAmount: invoice?.totalAmount ?? '',
  };

  const [invoiceData, setInvoiceData] = useState(initialInvoiceState);
  const [chosenCar, setChosenCar] = useState('');
  const isValid = isCurrentUserAnAdmin() || isCurrentUserAnOwner();
  const isEditable = (isValid && operation === 'edit') || operation === 'add';

  const typesOfWork = [
    'Autohound Repairs',
    'Other Garage Repairs',
    'Body Repair',
    'Performance Enhancements',
  ];

  const formRequirements = {
    repairDescription: [...typesOfWork],
    totalAmount: [...typesOfWork],
    serviceDate: [...typesOfWork],
    invoiceNumber: ['Autohound Repairs'],
    mileage: ['Autohound Repairs'],
  };

  useEffect(() => {
    if (operation === 'edit') {
      const invoiceToSearchFor = invoice._id;
      const chosenCar = cars.filter(car => car.invoices.includes(invoiceToSearchFor))[0];

      setChosenCar(chosenCar._id);
    }
  }, [cars, invoice?._id, invoiceData?.date, operation]);

  const { userId } = useParams();

  const setRequirement = requirementsArr => {
    return requirementsArr.includes(invoiceData.type);
  };

  const updateState = e => {
    const key = e.target.name;
    const value = e.target.value;

    setInvoiceData({ ...invoiceData, [key]: value });
  };

  const submitHandler = e => {
    e.preventDefault();
    handler(chosenCar, invoiceData);
  };

  return (
    <>
      {cars.length > 0 ? (
        <OnePage background="background">
          <div className="container px-2 pt-10 pb-40 w-2/7 h-hero" key={oilChange}>
            <div className="flex flex-col h-auto px-2 pt-10 mt-20 w-100">
              <FormHeader title={title} backLink={`/customer/${userId}`} />
              <form className="flex flex-col px-5 pt-10" onSubmit={e => submitHandler(e)}>
                <Row>
                  <SelectInput
                    label="Vehicle worked on"
                    name="car"
                    value={chosenCar}
                    onChange={e => setChosenCar(e.target.value)}
                    required={true}
                    defaultOption="Select a car"
                  >
                    {cars.map((car, i) => (
                      <option key={i.toString()} value={car._id}>
                        {car.make} {car.model} - {car.vin}
                      </option>
                    ))}
                  </SelectInput>
                </Row>
                <Row>
                  <SelectInput
                    label="Type of work"
                    name="type"
                    value={invoiceData.type}
                    onChange={e => updateState(e)}
                    required={true}
                    defaultOption="Select type of work"
                    disabled={!isEditable && invoiceData.type === 'Autohound Repairs'}
                  >
                    {typesOfWork.map((work, i) => (
                      <option key={i.toString()} value={work}>
                        {work}
                      </option>
                    ))}
                  </SelectInput>
                </Row>
                <Row>
                  <DateInput
                    label="Service Date"
                    name="date"
                    value={moment(invoiceData.date).format('YYYY-MM-DD')}
                    onChange={e => updateState(e)}
                    required={setRequirement(formRequirements.serviceDate)}
                  />
                </Row>
                <Row>
                  <TextInput
                    label="Invoice Number"
                    name="number"
                    onChange={e => updateState(e)}
                    value={invoiceData.number}
                    required={setRequirement(formRequirements.invoiceNumber)}
                  />
                  <NumberInput
                    label="Mileage"
                    name="mileage"
                    onChange={e => updateState(e)}
                    value={invoiceData.mileage}
                    required={setRequirement(formRequirements.mileage)}
                  />
                </Row>
                <Row>
                  <TextArea
                    label="Repair Description"
                    name="repairDescription"
                    onChange={e => updateState(e)}
                    value={invoiceData.repairDescription}
                    required={setRequirement(formRequirements.repairDescription)}
                  />
                </Row>
                {invoiceData.type === 'Autohound Repairs' && (
                  <>
                    <Row>
                      <SelectInput
                        label="Current oil change count"
                        name="oilChange"
                        value={invoiceData.oilChange}
                        onChange={e => updateState(e)}
                        required={true}
                        defaultOption="Select current oil change count"
                        disabled={!isEditable && invoiceData.type === 'Autohound Repairs'}
                      >
                        {[0, 1, 2, 3, 4, 5].map((count, i) => (
                          <option key={i.toString()} value={count}>
                            {count}
                          </option>
                        ))}
                      </SelectInput>
                    </Row>
                    {invoiceData.oilChange == 5 && (
                      <Row>
                        <SelectInput
                          label="Free oil change?"
                          name="freeOilChange"
                          value={invoiceData.freeOilChange}
                          onChange={e => updateState(e)}
                          required={false}
                          defaultOption="Choose Yes or No"
                          disabled={!isEditable && invoiceData.type === 'Autohound Repairs'}
                        >
                          <option value={true}>Yes</option>
                          <option value={false}>No</option>
                        </SelectInput>
                      </Row>
                    )}
                  </>
                )}
                <Row>
                  <CurrencyInput
                    label="Total Amount"
                    onChange={e => updateState(e)}
                    value={invoiceData.totalAmount}
                    name="totalAmount"
                    required={setRequirement(formRequirements.totalAmount)}
                    step="0.01"
                  />
                </Row>
                {!isValid && invoiceData.type === 'Autohound Repairs' && (
                  <BasicError customError="You do not have the authority to modify an Autohound invoice. Please choose a different type of invoice." />
                )}
                <button
                  type="submit"
                  className={`px-5 py-3 mt-5 w-36 text-white text-button rounded-sm hover:bg-opacity-80 ${
                    !isValid && invoiceData.type === 'Autohound Repairs'
                      ? 'bg-gray-2 hover:bg-opacity-100 cursor-default'
                      : 'bg-primary'
                  }`}
                  disabled={!isValid && invoiceData.type === 'Autohound Repairs'}
                >
                  {operation === 'add' ? 'Add Invoice' : 'Save Changes'}
                </button>
              </form>
            </div>
          </div>
        </OnePage>
      ) : (
        <h1>error</h1>
      )}
    </>
  );
};

InvoiceFields.propTypes = {
  cars: PropTypes.array,
  garages: PropTypes.array,
  title: PropTypes.string,
  operation: PropTypes.string,
  handler: PropTypes.func,
  invoice: PropTypes.object,
  id: PropTypes.string,
  oilChange: PropTypes.number,
};

export default InvoiceFields;
