import React from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';

import { DataCell, LinkRow } from '../../../components/Table';

const Invoice = ({ invoice }) => {
  const { id } = useParams();
  const date = new Date(invoice.date);
  const formattedDate = `${date.getUTCMonth() + 1}/${date.getUTCDate()}/${date.getUTCFullYear()}`;

  return (
    <LinkRow to={`/customer/${id}/invoice/${invoice._id}/edit`}>
      <DataCell label="Date" text={formattedDate} />
      <DataCell label="Repair Description" text={invoice.repairDescription} />
      <DataCell label="Vehicle" text={invoice.vehicle} />
      <DataCell label="Repair Total" text={`$${invoice.totalAmount}`} />
      <DataCell label="Invoice no" text={invoice.number} />
      <DataCell label="Mileage" text={invoice.mileage} />
      <DataCell label="Discount received" text={invoice.discount} />
      <DataCell label="Work done" text={invoice.type} />
    </LinkRow>
  );
};

Invoice.propTypes = {
  invoice: PropTypes.object,
  customerId: PropTypes.string,
};

export default Invoice;
