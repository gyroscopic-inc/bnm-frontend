import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import InvoiceFields from './InvoiceFields';
import { BasicError } from '../../../components/Error';

import UserAPIs from '../../../api/user';
import InvoiceAPIs from '../../../api/invoice';

import useTitle from '../../../hooks/useTitle';

const AddInvoice = () => {
  useTitle('Add Invoice');

  const [vehicles, setVehicles] = useState([]);
  const [oilChange, setOilChange] = useState(0);
  const [error, setError] = useState({
    message: '',
  });

  const { userId } = useParams();
  const history = useHistory();

  useEffect(() => {
    (async () => {
      try {
        const response = await UserAPIs.getCustomer(userId);
        const { vehicles, oilChange } = response.data.user.customerId;
        const userHasVehicles = !!vehicles && vehicles.length > 0;

        if (!userHasVehicles) {
          setError({ message: 'User has no vehicles and therefore cannot add an invoice.' });
        } else {
          setOilChange(oilChange);
          setVehicles([...vehicles]);
        }
      } catch (e) {
        setError(e);
      }
    })();
  }, [userId]);

  const addInvoice = async (chosenCarId, invoiceData) => {
    try {
      await InvoiceAPIs.addInvoice(userId, chosenCarId, invoiceData);
      history.push(`/customer/${userId}`);
    } catch (e) {
      setError({ message: e.response.data.error });
      console.error(e);
    }
  };

  return (
    <div className="grid">
      {vehicles.length > 0 ? (
        <InvoiceFields
          operation="add"
          title="Add Invoice"
          customerId={userId}
          cars={vehicles}
          handler={addInvoice}
          oilChange={oilChange}
        />
      ) : (
        <BasicError error={error} />
      )}
    </div>
  );
};

export default AddInvoice;
