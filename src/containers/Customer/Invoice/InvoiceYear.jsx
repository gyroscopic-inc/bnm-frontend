import React, { useState } from 'react';
import { useParams } from 'react-router';
import PropTypes from 'prop-types';

import Invoice from './Invoice';

const InvoiceYear = ({ invoicesForTheYear, year }) => {
  const [clicked, setClicked] = useState(false);
  const { id } = useParams();

  return (
    <div className="mt-3 cursor-pointer">
      <div
        className="p-5 text-secondary bg-secondary hover:bg-opacity-80"
        onClick={() => setClicked(!clicked)}
      >
        <p className="text-subtitle-2 text-text font-semibold">{year}</p>
      </div>
      <div className={`bg-white h-auto ${clicked ? '' : 'hidden'}`}>
        <table className="w-full table-auto">
          <tbody>
            {invoicesForTheYear.map((invoice, x) => (
              <Invoice key={x.toString()} customerId={id} invoice={invoice} />
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

InvoiceYear.propTypes = {
  invoicesForTheYear: PropTypes.array,
  year: PropTypes.string,
};

export default InvoiceYear;
