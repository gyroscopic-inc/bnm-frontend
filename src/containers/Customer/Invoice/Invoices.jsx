import React from 'react';
import { Link, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FiPlus } from 'react-icons/fi';

import InvoiceYear from './InvoiceYear';

const Invoices = ({ invoices }) => {
  const { id } = useParams();

  return (
    <div className="m-28 mt-16">
      <div className="flex flex-col">
        <div className="items-center">
          <div className="float-left mt-3">
            <h6 className="font-bold text-h6 text-text">Invoices</h6>
            <p className="text-sm text-text">Hover and click an invoice to edit.</p>
          </div>
          <div className="float-right">
            <button className="px-4 py-2 text-white rounded-sm bg-primary hover:bg-opacity-80">
              <Link to={`/customer/${id}/invoice/add`}>
                <FiPlus className="inline-block mr-2 text-base align-middle" />
                <p className="inline-block align-middle">Add Invoice</p>
              </Link>
            </button>
          </div>
        </div>
        <div className="flex flex-col mt-5">
          {Object.entries(invoices)?.length > 0 ? (
            <>
              {Object.keys(invoices).map((year, i) => (
                <InvoiceYear key={i.toString()} invoicesForTheYear={invoices[year]} year={year} />
              ))}
            </>
          ) : (
            <h1 className="text-text">No invoices found.</h1>
          )}
        </div>
      </div>
    </div>
  );
};

Invoices.propTypes = {
  invoices: PropTypes.object,
  customerId: PropTypes.string,
};

export default Invoices;
