import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';

import InvoiceFields from './InvoiceFields';
import InvoiceAPIs from '../../../api/invoice';
import VehicleAPIs from '../../../api/vehicle';
import UserAPIs from '../../../api/user';

import useTitle from '../../../hooks/useTitle';

const EditInvoice = () => {
  useTitle('Edit Invoice');

  const [vehicles, setVehicles] = useState([]);
  const [invoice, setInvoice] = useState({});
  const [oilChange, setOilChange] = useState(0);
  const [error, setError] = useState({
    message: '',
  });

  const { userId, invoiceId } = useParams();
  const history = useHistory();

  useEffect(() => {
    (async () => {
      try {
        const [invoiceResponse, vehiclesResponse, userResponse] = await Promise.all([
          InvoiceAPIs.getInvoice(invoiceId),
          VehicleAPIs.getUserVehicles(userId),
          UserAPIs.getCustomer(userId),
        ]);

        setOilChange(userResponse.data.user.customerId.oilChange);
        setVehicles(vehiclesResponse?.data?.vehicles);
        setInvoice(invoiceResponse?.data?.invoice);
      } catch (e) {
        setError(e);
      }
    })();
  }, [invoiceId, userId]);

  const updateInvoice = async (vehicleId, updates) => {
    try {
      if (invoice.type !== 'Autohound Repairs') {
        delete updates.oilChange;
      }

      await InvoiceAPIs.editInvoice(userId, vehicleId, invoiceId, updates);
      history.push(`/customer/${userId}`);
    } catch (e) {
      setError(e);
    }
  };

  return (
    <div className="grid">
      {vehicles?.length > 0 && Object.entries(invoice)?.length > 0 ? (
        <InvoiceFields
          operation="edit"
          title="Edit Invoice"
          invoice={invoice}
          handler={updateInvoice}
          cars={vehicles}
          oilChange={oilChange}
        />
      ) : (
        <div className="mt-10 place-self-center">
          <p className="text-xl font-semibold text-text">{error.message}</p>
        </div>
      )}
    </div>
  );
};

export default EditInvoice;
