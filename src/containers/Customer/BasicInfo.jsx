import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FiEdit2 } from 'react-icons/fi';

const BasicInfo = ({ user }) => (
  <div className="w-1/4 border-r border-gray-3 h-auto">
    <div className="mx-20 my-28 flex flex-col">
      <div className="flex flex-row items-center">
        <h6 className="text-text text-h6 font-bold mr-10">
          {user.customerId.userId.firstName} {user.customerId.userId.lastName}
        </h6>
        <Link to={`/customer/${user._id}/update`}>
          <FiEdit2 className="text-base" />
        </Link>
      </div>
      <div className="flex flex-row items-center">
        <span
          className={`rounded-full h-1.5 w-1.5 ${user.customerId.status ? 'bg-green' : 'bg-error'} mr-2`}
        ></span>
        <p className="text-subtitle-2 text-gray-1 font-semibold">
          {user.customerId.status ? 'Active' : 'Inactive'}
        </p>
      </div>
      <div className="flex flex-col mt-5">
        <p className="text-body-2 text-text font-normal">{user.customerId.userId.mobile}</p>
        <p className="text-body-2 text-text font-normal">{user.customerId.userId.email}</p>
      </div>
      <div className="flex flex-col mt-5 w-44">
        <span className="mb-1">
          <p className="text-body-2 text-text font-semibold">
            Shop Labour Rate: ${Number(user.customerId.labourRate || 0).toFixed(2)}
          </p>
          <p className="text-body-2 text-text font-semibold">
            Discount: ${Number(user.customerId.discount || 0).toFixed(2)}
          </p>
        </span>
        <hr className="text-text" />
        <span className="mt-1">
          <p className="text-body-2 text-text font-semibold">
            Labour Rate: ${(user.customerId.labourRate - user.customerId.discount || 0).toFixed(2)}
          </p>
        </span>
      </div>
      <div className="mt-5">
        <p className="text-body-2 text-text">
          <b>Notes: </b>
          {user.customerId.notes}
        </p>
      </div>
      <div className="mt-5">
        <p className="text-body-2 text-text">
          <b>Oil change: </b>
          {user.customerId.oilChange}
        </p>
      </div>
      <div className="mt-5">
        <p className="text-text">
          {user.customerId.freeOilChange && (
            <p>{`${user.customerId.userId.firstName} your next oil change is free up to $50.00`}</p>
          )}
        </p>
      </div>

      <div className="mt-10">
        <Link
          to={`/reports/${user._id}`}
          className="text-primary font-bold text-button p-4 rounded-sm border border-primary hover:rounded-none"
        >
          View Reports
        </Link>
      </div>
    </div>
  </div>
);

BasicInfo.propTypes = {
  user: PropTypes.object,
};

export default BasicInfo;
