import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import UserAPIs from '../../api/user';

import CustomerFields from './CustomerFields';
import { BasicError } from '../../components/Error';

import useTitle from '../../hooks/useTitle';

const EditCustomer = () => {
  useTitle('Edit Customer');
  const [error, setError] = useState({});
  const [customer, setCustomer] = useState({});

  const history = useHistory();
  const { id } = useParams();

  const updateCustomer = async customerDetails => {
    try {
      const { firstName, lastName, company, email, mobile, customerId } = customerDetails;

      await UserAPIs.updateCustomer(
        {
          firstName,
          lastName,
          company,
          email,
          mobile,
          customerData: {
            city: customerId.city,
            province: customerId.province,
            street: customerId.street,
            postalCode: customerId.postalCode,
            labourRate: customerId.labourRate || 0,
            notes: customerId.notes,
            status: customerId.status,
            discount: customerId.discount || 0,
          },
        },
        customerDetails._id
      );
      history.push(`/customer/${id}`);
    } catch (e) {
      setError(e);
    }
  };

  useEffect(() => {
    (async () => {
      try {
        const response = await UserAPIs.getCustomer(id);
        const { user } = response.data;

        setCustomer({ ...user });
      } catch (e) {
        setError(e);
      }
    })();
  }, [id]);

  return (
    <>
      {Object.keys(customer).length > 0 ? (
        <CustomerFields
          title="Edit Customer"
          operation="edit"
          submitHandler={updateCustomer}
          data={customer}
        />
      ) : (
        <BasicError error={error} />
      )}
    </>
  );
};

export default EditCustomer;
