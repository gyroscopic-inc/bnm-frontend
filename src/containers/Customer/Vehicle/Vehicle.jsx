import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { FiEdit2 } from 'react-icons/fi';
import PropTypes from 'prop-types';

import DropDownText from './DropdownText';

const Vehicle = ({ vehicle, customerId }) => {
  const [clicked, setClicked] = useState(false);

  const items = [
    { Driver: vehicle?.driverName },
    { Model: vehicle?.model },
    { 'A/C': `${vehicle.ac ? 'Yes' : 'No'}` },
    { 'Initial Cost': vehicle?.initialCost },
    { 'Purchase year': vehicle?.purchaseYear },
    { 'Year Manufactured': vehicle?.yearManufactured },
    { 'Body Style': vehicle?.body },
    { 'Engine Size': `${vehicle?.engine} L` },
    { VIN: vehicle?.vin },
    { Make: vehicle?.make },
    { Color: vehicle?.color },
    { Transmission: vehicle?.transmission },
  ];

  return (
    <div className="mt-3 cursor-pointer">
      <div
        className="flex flex-col p-5 text-secondary bg-secondary hover:bg-opacity-80"
        onClick={() => setClicked(!clicked)}
      >
        <div className="items-center">
          <p className="float-left font-semibold text-subtitle-2 text-text">
            ({vehicle.active ? 'Active' : 'Inactive'}) {vehicle.make} {vehicle.model} - {vehicle.vin}
            {`${vehicle.plateNumber ? ` - ${vehicle.plateNumber}` : ''}`}
            {`${vehicle.yearManufactured ? ` - ${vehicle.yearManufactured}` : ''}`}
          </p>
          <Link
            to={`/customer/${customerId}/vehicle/${vehicle._id}/update`}
            className="float-right text-sm font-semibold text-subtext"
          >
            <FiEdit2 className="text-base" />
          </Link>
        </div>
      </div>
      <div className={`bg-white h-auto p-5 grid grid-cols-3 gap-4 ${clicked ? '' : 'hidden'}`}>
        {items.map((item, i) => {
          const label = Object.keys(item)[0];
          const value = item[label];

          return <DropDownText key={i.toString()} label={label} value={value} />;
        })}
      </div>
    </div>
  );
};

Vehicle.propTypes = {
  vehicle: PropTypes.object,
  customerId: PropTypes.string,
};

export default Vehicle;
