import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import UserAPIs from '../../api/user';

import CustomerFields from './CustomerFields';

import useTitle from '../../hooks/useTitle';

const AddCustomer = () => {
  useTitle('Add Customer');
  const [error, setError] = useState('');
  const history = useHistory();

  const defaultData = {
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    customerId: {
      city: '',
      province: '',
      street: '',
      postalCode: '',
      labourRate: '',
      notes: '',
      status: true,
    },
    role: 'customer',
  };

  const addCustomer = async customer => {
    try {
      customer['customerData'] = customer['customerId'];
      delete customer['customerId'];

      await UserAPIs.addCustomer(customer);
      history.push('/dashboard');
    } catch (e) {
      const hasDetails = !!e.response.data.error.details && e.response.data.error.details.length > 0;

      if (!hasDetails) {
        setError(e?.response?.data?.error);
      } else {
        setError(e.response.data.error.details[0].message);
      }

      console.log(e.message);
    }
  };

  return (
    <CustomerFields title="Add a customer" submitHandler={addCustomer} data={defaultData} error={error} />
  );
};

export default AddCustomer;
