import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FiPlus } from 'react-icons/fi';

import Vehicle from './Vehicle/Vehicle';

const Vehicles = ({ vehicles, id }) => (
  <div className="m-28 mb-16">
    <div className="flex flex-col">
      <div className="items-center">
        <div className="float-left mt-3">
          <h6 className="text-h6 text-text font-bold">Vehicles</h6>
        </div>
        <div className="float-right">
          <button className="bg-primary hover:bg-opacity-80 py-2 px-4 text-white rounded-sm">
            <Link to={`/customer/${id}/vehicle/add`}>
              <FiPlus className="inline-block align-middle mr-2 text-base" />
              <p className="inline-block align-middle">Add Vehicle</p>
            </Link>
          </button>
        </div>
      </div>
      <div className="flex flex-col mt-5">
        {vehicles && vehicles.length > 0 ? (
          <>
            {vehicles.map((vehicle, i) => (
              <Vehicle key={i.toString()} vehicle={vehicle} customerId={id} />
            ))}
          </>
        ) : (
          <>
            <h1 className="text-text">No vehicles found.</h1>
          </>
        )}
      </div>
    </div>
  </div>
);

Vehicles.propTypes = {
  vehicles: PropTypes.array,
  id: PropTypes.string,
};

export default Vehicles;
