import React from 'react';
import PropTypes from 'prop-types';

const ReportTable = ({ reports, chosenReport }) => {
  const reportNames = [
    'Autohound Report',
    'Additional Vehicle Cost Report',
    'Collision & Modification Report',
    // 'Business Use Tracker Report',
  ];

  const headers = {
    0: ['Repairs', 'RCPA', 'Global RCPA'],
    1: ['Initial Vehicle Expense', 'Other Shop Repairs'],
    2: ['Body Repair', 'Performance Enhancements'],
    // 3: [
    //   'Insurance Cost',
    //   'License Fees',
    //   'Fuel',
    //   'Mileage In',
    //   'Mileage Out',
    //   'Mileage',
    //   'Total Business Expense',
    // ],
  };

  const reportTypes = Object.keys(reports);
  const currentReportName = reportTypes[chosenReport];
  const currentReport = reports[currentReportName] ?? {};
  const { finalTotals } = currentReport;
  const yearsInReports = currentReport.reports.map(row => row.year);

  return (
    <>
      {!!currentReport.reports && Object.entries(currentReport.reports).length > 0 ? (
        <table className="my-10 table-auto">
          <thead>
            <tr className="p-5 font-medium text-left bg-white border-b text-text text-subtitle-2 border-secondary">
              <th className="p-5 text-center cursor-pointer">Year</th>
              {headers[chosenReport].map((header, i) => (
                <th key={i.toString()} className="p-5 text-center cursor-pointer">
                  {header}
                </th>
              ))}
            </tr>
            <tr className="p-5 font-medium text-left bg-white border-b text-primary text-body-2 border-secondary">
              <th className="p-5 text-center cursor-pointer">Totals</th>
              {Object.keys(finalTotals).map((currentKey, i) => {
                const isAutohoundReport = currentReportName === 'autohoundReport';
                if (isAutohoundReport) {
                  return (
                    <th key={i.toString()} className="p-5 text-center cursor-pointer">
                      {currentKey === 'total'
                        ? `$${finalTotals[currentKey]}`
                        : `scroll down to specific year for the ${headers[0][i]}`}
                    </th>
                  );
                } else {
                  return (
                    <th key={i.toString()} className="p-5 text-center cursor-pointer">
                      {`$${finalTotals[currentKey]}`}
                    </th>
                  );
                }
              })}
            </tr>
          </thead>
          <tbody>
            {currentReport.reports.map((report, i) => {
              const categories = Object.keys(report).filter(keys => keys !== 'year');

              return (
                <tr key={i} className="bg-white border-b hover:bg-gray-4 border-secondary">
                  <td className="p-5 font-normal text-center text-gray-1 text-body-2">{yearsInReports[i]}</td>
                  {categories.map((category, x) => (
                    <td
                      key={x.toString()}
                      className="p-5 font-normal text-center text-gray-1 text-body-2"
                    >{`${report[category] === null ? '-' : '$' + report[category].toFixed(2)}`}</td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : (
        <p className="mt-10 text-sm text-center text-error">
          No reports for the category: {reportNames[chosenReport]}
        </p>
      )}
    </>
  );
};

ReportTable.propTypes = {
  chosenReport: PropTypes.number,
  reports: PropTypes.object,
};

export default ReportTable;
