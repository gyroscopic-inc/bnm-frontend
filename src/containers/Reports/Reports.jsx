import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';

import { FiArrowLeft } from 'react-icons/fi';

import OnePage from '../../components/Pages/OnePage';
import { BasicError, PageError } from '../../components/Error';

import ReportTable from './ReportTable';

import UserAPIs from '../../api/user';
import ReportAPIs from '../../api/report';

import useTitle from '../../hooks/useTitle';
import { useCallback } from 'react';

// import { isCurrentUserACustomer } from '../../utilities/authentication';

const Reports = () => {
  useTitle('Reports');

  const [chosenReport, setChosenReport] = useState(0);
  const [chosenCar, setChosenCar] = useState('');
  const [reports, setReports] = useState({});
  const [error, setError] = useState({});
  const [customerDetails, setCustomerDetails] = useState({});

  let reportTabs = [
    'Autohound Report',
    'Additional Vehicle Cost Report',
    'Collision & Modification Report',
    // 'Business Use Tracker Report',
  ];

  // if (isCurrentUserACustomer()) {
  //   reportTabs.pop();
  // }

  const { id } = useParams();

  const getCarReports = useCallback(async () => {
    try {
      const response = await ReportAPIs.getCarReports(customerDetails?.userId?._id, chosenCar);

      setReports(response?.data?.reports);
    } catch (e) {
      const error = e?.response?.data?.error ?? e.message;

      setError({ message: error });
    }
  }, [customerDetails?.userId?._id, chosenCar]);

  useEffect(() => {
    (async () => {
      try {
        const response = await UserAPIs.getCustomer(id);

        setCustomerDetails({ ...response?.data?.user?.customerId });
      } catch (e) {
        setError(e);
      }
    })();
  }, [id]);

  useEffect(() => {
    const hasChosenAVehicle = customerDetails?._id && chosenCar && chosenCar !== 'Select Car';

    if (hasChosenAVehicle) {
      getCarReports();
    }
  }, [chosenCar, customerDetails?._id, getCarReports]);

  return (
    <>
      <OnePage>
        {customerDetails?.vehicles?.length > 0 &&
        customerDetails?.vehicles?.filter(vehicle => vehicle?.invoices.length > 0).length > 0 ? (
          <div className="px-20 flex flex-col relative pt-28">
            <div className="flex justify-between my-2">
              <div className="flex flex-row float-left">
                <Link to={`/customer/${id}`}>
                  <FiArrowLeft className="text-xl" />
                </Link>
              </div>
              <div>
                <h1 className="font-semibold text-h5 text-text">Reports</h1>
              </div>
              <div className="float-right"></div>
            </div>
            <select
              className="p-4 mt-1 mb-4 appearance-none cursor-pointer bg-secondary text-text hover:bg-opacity-80"
              defaultValue={chosenCar}
              onChange={e => setChosenCar(e.target.value)}
            >
              <option>Select Car</option>
              {customerDetails.vehicles.map((car, i) => (
                <option key={i.toString()} value={car._id}>
                  {car.make} {car.model} - {car.vin}
                </option>
              ))}
            </select>
            <div className={`grid grid-cols-${reportTabs.length} gap-4`}>
              {reportTabs.map((tab, i) => (
                <button
                  key={i.toString()}
                  onClick={() => setChosenReport(i)}
                  className="p-4 text-text bg-secondary hover:bg-opacity-80"
                >
                  {tab}
                </button>
              ))}
            </div>
            {Object.entries(reports)?.length > 0 ? (
              <ReportTable reports={reports} chosenReport={chosenReport} />
            ) : (
              <BasicError error={error} />
            )}
          </div>
        ) : (
          <PageError message="Cannot generate reports: There are no vehicles and/or invoices found" />
        )}
      </OnePage>
    </>
  );
};

Reports.propTypes = {
  cars: PropTypes.array,
};

export default Reports;
