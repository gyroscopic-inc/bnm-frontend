import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import OnePage from '../../components/Pages/OnePage';
import Box from '../../components/Box/Box';

import UserAPIs from '../../api/user';
import { setToken, isTokenValid } from '../../utilities/token';

import useTitle from '../../hooks/useTitle';

const Signin = () => {
  useTitle('Signin');
  const [credentials, setCredentials] = useState({ email: '', password: '' });
  const [error, setError] = useState('');

  const history = useHistory();

  const resetState = () => {
    setCredentials({ email: '', password: '' });
  };

  const login = async e => {
    e.preventDefault();
    resetState();

    try {
      const response = await UserAPIs.loginUser(credentials);
      const { user } = response.data.data;

      setToken(user.token);

      const hasValidToken = isTokenValid(user.token);

      if (!hasValidToken) {
        setError({ message: 'no valid token. ' });
      }

      const isCustomer = user.role === 'customer';
      const isGarageOwner = user.role === 'owner';
      const isFacilityStaff = user.role === 'admin';

      if (isCustomer) history.push(`/customer/${user._id}`);
      if (isGarageOwner) history.push(`/dashboard`);
      if (isFacilityStaff) history.push(`/admin/dashboard`);
    } catch (err) {
      console.log({ err });
      setError(err.response.data.error);
    }
  };

  return (
    <OnePage className="plans">
      <div className="container w-2/6 px-8 pt-32 pb-40 h-hero">
        <Box title="Log In">
          <div className="mt-10">
            <form className="flex flex-col" onSubmit={e => login(e)}>
              <label className="text-small text-text">Email</label>
              <input
                onChange={e => setCredentials({ ...credentials, email: e.target.value })}
                value={credentials.email}
                type="email"
                className="px-4 py-3 mt-1 bg-background text-body-2 text-subtext"
                name="email"
                required
              />
              <label className="mt-2 text-small text-text">Password</label>
              <input
                onChange={e => setCredentials({ ...credentials, password: e.target.value })}
                value={credentials.password}
                type="password"
                className="px-4 py-3 mt-1 bg-background text-body-2 text-subtext"
                name="password"
                required
              />
              <button
                type="submit"
                className="px-5 py-3 mt-10 text-white rounded-sm bg-primary hover:bg-primary-light"
              >
                Login
              </button>
              <Link
                to="/forgot-password"
                className="mt-3 text-sm font-medium text-center underline text-gray-1"
              >
                Forgot password?
              </Link>
            </form>
            {error && (
              <div className="mt-5">
                <p className="text-sm text-center text-error">{error}</p>
              </div>
            )}
          </div>
        </Box>
      </div>
    </OnePage>
  );
};

Signin.propTypes = {
  history: PropTypes.object,
};

export default Signin;
