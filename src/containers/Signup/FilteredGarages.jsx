import React from 'react';
import PropTypes from 'prop-types';

const FilteredGarages = ({ garages, setChosenGarage }) =>
  garages.map((garage, index) => (
    <button
      key={index}
      type="button"
      className="bg-background px-4 py-3 text-text font-normal text-caption text-left"
      onClick={e => setChosenGarage(e.target.innerText)}
    >
      {garage.name}
    </button>
  ));

FilteredGarages.propTypes = {
  garages: PropTypes.array,
  setChosenGarage: PropTypes.func,
};

export default FilteredGarages;
