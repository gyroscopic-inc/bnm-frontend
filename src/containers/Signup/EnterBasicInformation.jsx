import React, { useContext, useState } from 'react';

import { SignupContext } from './Signup';
import UserAPIs from '../../api/user';

import Box from '../../components/Box/Box';

const EnterBasicInformation = () => {
  const [error, setError] = useState({});
  const { step, goToNextStep, updateUserData, data } = useContext(SignupContext);

  const updateInputValue = e => {
    const key = e.target.name;
    const value = e.target.value;

    updateUserData(key, value);
  };

  const checkEmailAndGoToNextIfValid = async () => {
    try {
      const response = await UserAPIs.checkIfUserWithEmailExists(data.email);

      if (!response.data.userExists) {
        goToNextStep();
      } else {
        setError({ message: `User with email: ${data.email} already exists.` });
      }
    } catch (e) {
      setError(e);
    }
  };

  return (
    <Box title="Basic Information" progress={step}>
      <div className="container">
        <form className="flex flex-col">
          <div className="flex flex-row">
            <div className="flex flex-col w-full mr-5">
              <label className="text-small text-text">First Name</label>
              <input
                onChange={e => updateInputValue(e)}
                value={data.firstName}
                type="text"
                className="px-4 py-3 mt-1 mb-4 bg-background text-body-2 text-subtext"
                name="firstName"
                required
              />
            </div>
            <div className="flex flex-col w-full">
              <label className="text-small text-text">Last Name</label>
              <input
                onChange={e => updateInputValue(e)}
                value={data.lastName}
                type="text"
                className="px-4 py-3 mt-1 mb-4 bg-background text-body-2 text-subtext"
                name="lastName"
                required
              />
            </div>
          </div>
          <div className="flex flex-col w-full">
            <label className="text-small text-text">Email</label>
            <input
              onChange={e => updateInputValue(e)}
              value={data.email}
              type="email"
              className="px-4 py-3 mt-1 mb-4 bg-background text-body-2 text-subtext"
              name="email"
              required
            />
          </div>
          <label className="text-small text-text">Password</label>
          <input
            onChange={e => updateInputValue(e)}
            value={data.password}
            type="password"
            className="px-4 py-3 mt-1 mb-4 bg-background text-body-2 text-subtext"
            name="password"
            required
          />
          {Object.entries(error).length > 0 && <p className="text-sm text-error">{error.message}</p>}
          <button
            onClick={() => checkEmailAndGoToNextIfValid()}
            type="button"
            className="px-5 py-3 mt-5 text-white rounded-sm bg-primary hover:bg-primary-light"
          >
            Continue
          </button>
        </form>
      </div>
    </Box>
  );
};

export default EnterBasicInformation;
