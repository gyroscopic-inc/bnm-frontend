import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';

import { SignupContext } from './Signup';
import TermsUsage from './TermsUsage';

const TOS = () => {
  const history = useHistory();
  const { goToNextStep } = useContext(SignupContext);

  const goBackToHomePage = () => {
    history.push('/');
  };

  return (
    <div className="px-4">
      <form className="flex flex-col">
        <div className="overflow-auto h-100">
          <TermsUsage />
          <div className="flex">
            <button
              onClick={() => goToNextStep()}
              type="button"
              className="mr-2 px-5 py-3 mt-5 text-sm text-white rounded-sm space-y-4 bg-primary hover:bg-primary-light"
            >
              Agree
            </button>
            <button
              onClick={() => goBackToHomePage()}
              type="button"
              className="px-5 py-3 mt-5 text-sm text-white rounded-sm bg-primary hover:bg-primary-light"
            >
              Disagree
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default TOS;
