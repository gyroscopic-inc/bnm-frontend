import React, { useContext, useState } from 'react';

import UserAPIs from '../../api/user';

import { SignupContext } from './Signup';
import Box from '../../components/Box/Box';

const EnterCode = () => {
  const [error, setError] = useState({});
  const [passcode, setPasscode] = useState('');

  const { step, data, goToNextStep } = useContext(SignupContext);

  const validatePasscode = async e => {
    e.preventDefault();
    const body = {
      mobile: data.mobile,
      passcode,
    };

    try {
      await UserAPIs.verifyEnteredPasscode(body);
      goToNextStep();
    } catch (err) {
      setPasscode('');
      setError(err?.response?.data?.error);
    }
  };

  return (
    <Box title="Enter Code" subtitle="Please enter the verification code below." progress={step}>
      <div className="mt-10">
        <form className="flex flex-col">
          <label className="text-small text-text">Verification Code</label>
          <input
            onChange={e => setPasscode(e.target.value)}
            type="text"
            name="passcode"
            value={passcode}
            pattern="[0-9]"
            className="px-4 py-3 my-2 bg-background text-body-2 text-subtext"
            required
          />
          <button
            onClick={e => validatePasscode(e)}
            className="px-5 py-3 text-white rounded-sm bg-primary hover:bg-primary-light"
          >
            Submit
          </button>
          {Object.entries(error).length > 0 && <p className="text-sm text-error">{error}</p>}
        </form>
      </div>
    </Box>
  );
};

export default EnterCode;
