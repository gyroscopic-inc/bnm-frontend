import React, { useState, useCallback, createContext } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';

import OnePage from '../../components/Pages/OnePage';

import EnterMobile from './EnterMobile';
import EnterCode from './EnterCode';
import EnterBasicInformation from './EnterBasicInformation';
import EnterPlanAndPayment from './EnterPlanAndPayment';
import TOS from './TOS';

import useTitle from '../../hooks/useTitle';

const START_AT_MOBILE_SIGNUP = 0;

export const SignupContext = createContext();

const Signup = () => {
  useTitle('Signup');

  const [step, setStep] = useState(START_AT_MOBILE_SIGNUP);
  const [data, setData] = useState({
    role: 'owner',
    mobile: '',
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    garage: '',
    chosenGarage: '',
  });

  const goToNextStep = () => {
    const FIRST_STEP = 0;
    const LAST_STEP = 4;
    const isWithinSignupRange = step >= FIRST_STEP && step <= LAST_STEP;

    if (isWithinSignupRange) {
      setStep(step + 1);
    }
  };

  const updateUserData = useCallback((key, value) => {
    setData(data => ({ ...data, [key]: value }));
  }, []);

  return (
    <SignupContext.Provider value={{ step, goToNextStep, data, updateUserData }}>
      <OnePage background="background">
        <div className="container w-2/6 px-8 pt-32 pb-40 h-hero">
          {
            {
              0: <EnterMobile />,
              1: <EnterCode />,
              2: <TOS />,
              3: <EnterBasicInformation />,
              4: <EnterPlanAndPayment />,
            }[step]
          }
        </div>
      </OnePage>
    </SignupContext.Provider>
  );
};

Signup.propTypes = {
  location: PropTypes.object.isRequired,
};

export default withRouter(Signup);
