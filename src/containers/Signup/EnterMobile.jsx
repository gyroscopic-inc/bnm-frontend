import React, { useContext, useState } from 'react';

import UserAPIs from '../../api/user';

import { SignupContext } from './Signup';
import Box from '../../components/Box/Box';

const EnterMobile = () => {
  const [error, setError] = useState({});
  const [mobile, setMobile] = useState('');

  const { step, goToNextStep, updateUserData } = useContext(SignupContext);

  const sendText = async () => {
    updateUserData('mobile', mobile);

    try {
      await UserAPIs.verifyMobile(mobile);
      goToNextStep();
    } catch (error) {
      setError(error?.response?.data?.error);
    }
  };

  return (
    <Box
      title="Verify Mobile Number"
      subtitle="We will send you a one time SMS message. Carrier rates may apply."
      progress={step}
    >
      <div className="mt-10">
        <form className="flex flex-col">
          <label className="text-small text-text">Mobile Number</label>
          <input
            onChange={e => setMobile(e.target.value)}
            value={mobile}
            type="tel"
            className="px-4 py-3 my-2 bg-background text-body-2 text-subtext"
            pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
            placeholder="000-000-0000"
            name="mobile"
            required
          />

          <button
            type="button"
            onClick={() => sendText()}
            className="px-5 py-3 text-white rounded-sm bg-primary hover:bg-primary-light"
          >
            Submit
          </button>
          {Object.entries(error).length > 0 && <p className="py-3 text-sm text-error">{error}</p>}
        </form>
      </div>
    </Box>
  );
};

export default EnterMobile;
