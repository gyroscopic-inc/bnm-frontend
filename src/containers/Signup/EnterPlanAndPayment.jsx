import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useStripe, useElements, CardElement } from '@stripe/react-stripe-js';

import { SignupContext } from './Signup';
import AppContext from '../../AppContext';

import Box from '../../components/Box/Box';

import UserAPIs from '../../api/user';
import PaymentAPIs from '../../api/payment';
import GarageAPIs from '../../api/garage';

const CARD_ELEMENT_OPTIONS = {
  style: {
    base: {
      color: '#898989',
      fontFamily: 'sans-serif',
      fontStyle: 'normal',
    },
    fontSmoothing: 'antialiased',
    ':-webkit-autofill': {
      color: '#fce883',
    },
    '::placeholder': {
      color: '#87BBFD',
    },
    invalid: {
      color: '#FB4E4E',
    },
  },
};

const EnterPlanAndPayment = () => {
  const [error, setError] = useState({});
  const [cardDetails, setCardDetails] = useState({});
  const [garages, setGarages] = useState([]);

  // const plans = ['Choose a plan', 'Basic License', 'Business Owner License', 'Professional Shop License'];
  const plans = ['Choose a plan', 'Basic License'];

  const history = useHistory();
  const { step, data, updateUserData } = useContext(SignupContext);
  const { plan, setPlan } = useContext(AppContext);

  const stripe = useStripe();
  const elements = useElements();

  const finishSignup = async e => {
    e.preventDefault();
    // setData({ ...data, ...cardDetails });

    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }

    try {
      const isPricedPlan = ['Business Owner License', 'Professional Shop License'].includes(plan);

      if (isPricedPlan) {
        const res = await PaymentAPIs.getPaySetup();
        const priceId = res.data.prices[plan];

        await createCustomerSubscription(data.email, priceId);
      }

      await registerUser();

      history.push('/dashboard');
    } catch (e) {
      setError(e?.response?.data?.error);
    }
  };

  const createCustomerSubscription = async (email, priceId) => {
    try {
      const { paymentMethod } = await stripe.createPaymentMethod({
        type: 'card',
        card: elements.getElement(CardElement),
        billing_details: {
          email,
        },
      });
      const { customer } = (await PaymentAPIs.createStripeCustomer(email, paymentMethod)).data;

      const subscriptionObj = {
        customerId: customer.id,
        paymentMethodId: paymentMethod.id,
        priceId,
      };

      return PaymentAPIs.createSubscription(subscriptionObj);
    } catch (e) {
      setError(e);
    }
  };

  const registerUser = () => {
    Object.keys(data).forEach(key => !data[key] && delete data[key]);
    return UserAPIs.registerUser(data);
  };

  const updateCard = (key, value) => {
    const card = Object.assign(cardDetails, { [key]: value });
    setCardDetails({ ...card });
  };

  useEffect(() => {
    if (plan === 'Basic License') {
      updateUserData('role', 'customer');
    }
  }, [plan, updateUserData]);

  useEffect(() => {
    (async () => {
      try {
        const response = await GarageAPIs.getGarages();
        setGarages(response.data.garages);
      } catch (error) {
        setError(error.message);
      }
    })();
  }, []);

  return (
    <Box title="Plan and Payment" progress={step}>
      <div>
        <form onSubmit={e => finishSignup(e)} className="flex flex-col">
          <label className="text-small text-text">Choose Plan</label>
          <select
            name="plan"
            className="px-4 py-3 mt-1 mb-4 bg-background text-body-2 text-subtext appearance-none"
            value={plan}
            onChange={e => setPlan(e.target.value)}
          >
            {plans.map((currPlan, i) => (
              <option key={i.toString()} value={currPlan}>
                {currPlan}
              </option>
            ))}
          </select>
          {plan?.length > 0 && (
            <>
              {plan === 'Basic License' && garages?.length > 0 && (
                <>
                  <label className="text-small text-text">Choose Garage</label>
                  <select
                    name="garage"
                    className="px-4 py-3 mt-1 mb-4 appearance-none bg-background text-body-2 text-subtext"
                    value={data.chosenGarage}
                    onChange={e => updateUserData('chosenGarage', e.target.value)}
                  >
                    <option>Choose a garage</option>
                    {garages.map(({ _id, name }, i) => (
                      <option key={i.toString()} value={_id}>
                        {name}
                      </option>
                    ))}
                  </select>
                </>
              )}
              {plan !== 'Basic License' && (
                <>
                  <label className="text-small text-text">Your Garage Name</label>
                  <input
                    onChange={e => updateUserData('garage', e.target.value)}
                    value={data.garage}
                    type="text"
                    className="px-4 py-3 mt-1 mb-4 bg-background text-body-2 text-subtext"
                    name="garage"
                    required
                  />
                  <label className="text-small text-text">Card Owner</label>
                  <input
                    onChange={e => updateCard(e.target.name, e.target.value)}
                    value={cardDetails.owner}
                    type="text"
                    className="px-4 py-3 mt-1 mb-4 bg-background text-body-2 text-subtext"
                    name="owner"
                    required
                  />
                  <label className="text-small text-text">Card Details</label>
                  <CardElement options={CARD_ELEMENT_OPTIONS} />
                </>
              )}
            </>
          )}
          {Object.entries(error).length > 0 && <p className="text-sm text-error">{error}</p>}
          <button
            type="submit"
            className="px-5 py-3 mt-5 text-white rounded-sm bg-primary hover:bg-primary-light"
          >
            Finish
          </button>
          <p className="mt-3 text-center text-small text-subtext">
            By clicking “Sign Up” you accept Terms of Use and Privacy Policy
          </p>
        </form>
      </div>
    </Box>
  );
};

export default EnterPlanAndPayment;
