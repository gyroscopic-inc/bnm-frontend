import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import OnePage from '../../components/Pages/OnePage';
import Box from '../../components/Box/Box';

import UserAPIs from '../../api/user';

const ForgotPassword = () => {
  const [email, setEmail] = useState('');
  const [error, setError] = useState('');
  const history = useHistory();

  const processForgotPassword = async () => {
    try {
      const response = await UserAPIs.processForgotPassword(email);
      localStorage.setItem('email', email);
      console.log(response);
      history.push(`/forgot-password/verify/${response.data.twilio.to}`);
    } catch (e) {
      setEmail('');
      setError(e.response.data.error);
    }
  };

  return (
    <OnePage className="plans">
      <div className="container w-2/6 px-8 pt-32 pb-40 h-hero">
        <Box title="Password Reset">
          <div className="mt-10 flex flex-col">
            <label className="text-small text-text">Email</label>
            <input
              onChange={e => setEmail(e.target.value)}
              value={email}
              type="email"
              className="px-4 py-3 mt-1 bg-background text-body-2 text-subtext"
              name="email"
              required
            />
            <button
              onClick={() => processForgotPassword()}
              type="button"
              className="px-5 py-3 mt-10 text-white rounded-sm bg-primary hover:bg-primary-light"
            >
              Continue
            </button>
            {error?.length > 0 && (
              <div className="mt-5">
                <p className="text-sm text-center text-error">{error}</p>
              </div>
            )}
          </div>
        </Box>
      </div>
    </OnePage>
  );
};

export default ForgotPassword;
