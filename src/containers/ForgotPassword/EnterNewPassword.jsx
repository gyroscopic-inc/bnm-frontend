import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import Box from '../../components/Box/Box';
import OnePage from '../../components/Pages/OnePage';

import UserAPIs from '../../api/user';

const EnterNewPassword = () => {
  const [newPassword, setNewPassword] = useState('');
  const [error, setError] = useState('');
  const history = useHistory();

  const updateUserPassword = async () => {
    try {
      const body = {
        email: localStorage.getItem('email'),
        password: newPassword,
      };
      const response = await UserAPIs.updateUserPassword(body);
      console.log(response);
      history.push('/signin');
    } catch (e) {
      setNewPassword('');
      setError(e.response.data.error);
    }
  };

  return (
    <OnePage className="plans">
      <div className="container w-2/6 px-8 pt-32 pb-40 h-hero">
        <Box title="Forgot Password" subtitle="Enter your new password">
          <div className="mt-10 flex flex-col">
            <label className="text-small text-text">New Password</label>
            <input
              onChange={e => setNewPassword(e.target.value)}
              name="password"
              value={newPassword}
              type="password"
              className="px-4 py-3 mt-1 bg-background text-body-2 text-subtext"
              required
            />
            {error?.length > 0 && <p className="text-sm text-error">{error}</p>}
            <button
              onClick={e => updateUserPassword(e)}
              className="mt-14 px-5 py-3 text-white rounded-sm bg-primary hover:bg-primary-light"
            >
              Submit
            </button>
          </div>
        </Box>
      </div>
    </OnePage>
  );
};

export default EnterNewPassword;
