import React, { useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';

import UserAPIs from '../../api/user';

import Box from '../../components/Box/Box';
import OnePage from '../../components/Pages/OnePage';

const VerifyPasscode = () => {
  const [error, setError] = useState('');
  const [passcode, setPasscode] = useState('');
  const { mobile } = useParams();
  const history = useHistory();

  const validatePasscode = async e => {
    e.preventDefault();
    const body = {
      mobile,
      passcode,
    };

    try {
      await UserAPIs.verifyEnteredPasscode(body);
      history.push('/forgot-password/update-password');
    } catch (err) {
      setPasscode('');
      setError(err.response.data.error);
    }
  };

  return (
    <OnePage className="plans">
      <div className="container w-2/6 px-8 pt-32 pb-40 h-hero">
        <Box title="Enter Code" subtitle={`Please enter the 6-digit verification sent to: ${mobile}`}>
          <div className="mt-10 flex flex-col">
            <label className="text-small text-text">Verification Code</label>
            <input
              onChange={e => setPasscode(e.target.value)}
              maxLength="6"
              name="passcode"
              value={passcode}
              type="text"
              className="px-4 py-3 mt-1 bg-background text-body-2 text-subtext"
              required
            />
            {error?.length > 0 && <p className="text-sm text-error">{error}</p>}
            <button
              onClick={e => validatePasscode(e)}
              className="mt-14 px-5 py-3 text-white rounded-sm bg-primary hover:bg-primary-light"
            >
              Submit
            </button>
          </div>
        </Box>
      </div>
    </OnePage>
  );
};

export default VerifyPasscode;
