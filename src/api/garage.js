import HttpService from '../services/http';

const getGarages = () => {
  const http = new HttpService(`/garage`);
  return http.unsecuredGet();
};

export default { getGarages };
