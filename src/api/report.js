import HttpService from '../services/http';

const getCarReports = (customerId, vehicleId) => {
  const http = new HttpService(`/report/${customerId}/${vehicleId}`);
  return http.get();
};

export default { getCarReports };
