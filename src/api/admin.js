import HttpService from '../services/http';

const getAllCustomers = () => {
  const http = new HttpService(`/admin/customers/all`);
  return http.get();
};

const updateGarageName = (garageId, newGarageName) => {
  const http = new HttpService(`/admin/garage/update`);
  return http.put({ garageId, newGarageName });
};

export default { updateGarageName, getAllCustomers };
