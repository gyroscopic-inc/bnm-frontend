import HttpService from '../services/http';

const getPaySetup = () => {
  const http = new HttpService(`/payment`);
  return http.unsecuredGet();
};

const createStripeCustomer = (email, paymentMethod) => {
  const http = new HttpService(`/payment/create-customer`);
  return http.unsecuredPost({ email, paymentMethod });
};

const createSubscription = ({ customerId, priceId }) => {
  const http = new HttpService(`/payment/create-subscription`);
  return http.unsecuredPost({ customerId, priceId });
};

export default { getPaySetup, createStripeCustomer, createSubscription };
