import HttpService from '../services/http';

const getUser = email => {
  const http = new HttpService(`/user/profile/${email}`);
  return http.get();
};

const verifyMobile = mobile => {
  const http = new HttpService(`/user/verify/mobile`);
  return http.post({ mobile });
};

const verifyEnteredPasscode = body => {
  const http = new HttpService(`/user/verify/passcode`);
  return http.post({
    mobile: body.mobile,
    passcode: body.passcode,
  });
};

const registerUser = body => {
  const http = new HttpService(`/user/register`);
  return http.post(body);
};

const loginUser = body => {
  const http = new HttpService(`/user/login`);
  return http.post(body);
};

const addCustomer = body => {
  const http = new HttpService(`/admin/customers`);
  return http.post(body);
};

const getCustomers = () => {
  const http = new HttpService(`/admin/customers`);
  return http.get();
};

const getCustomer = id => {
  const http = new HttpService(`/user/profile/${id}`);
  return http.get();
};

const updateCustomer = (body, userId) => {
  const http = new HttpService(`/user/profile/${userId}`);
  return http.put(body);
};

const checkIfUserWithEmailExists = email => {
  const http = new HttpService(`/user/check/${email}`);
  return http.unsecuredGet();
};

const processForgotPassword = email => {
  const http = new HttpService(`/user/forgot-password/${email}`);
  return http.unsecuredPost();
};

const updateUserPassword = body => {
  const http = new HttpService(`/user/update-password`);
  return http.unsecuredPost(body);
};

export default {
  getUser,
  verifyEnteredPasscode,
  verifyMobile,
  loginUser,
  addCustomer,
  getCustomers,
  getCustomer,
  registerUser,
  updateCustomer,
  checkIfUserWithEmailExists,
  processForgotPassword,
  updateUserPassword,
};
