import HttpService from '../services/http';

const getInvoice = invoiceId => {
  const http = new HttpService(`/invoice/${invoiceId}`);
  return http.get();
};

const editInvoice = (customerId, vehicleId, invoiceId, body) => {
  const http = new HttpService(`/invoice/${customerId}/${vehicleId}/${invoiceId}`);
  return http.put(body);
};

const addInvoice = (customerId, vehicleId, body) => {
  const http = new HttpService(`/invoice/${customerId}/${vehicleId}`);
  return http.post(body);
};

export default { getInvoice, addInvoice, editInvoice };
