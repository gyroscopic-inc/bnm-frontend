module.exports = {
  purge: ['src/**/*.js', 'src/**/*.jsx', 'public/**/*.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    listStyleType: {
      decimal: 'decimal',
      alpha: 'lower-alpha',
    },
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    colors: {
      primary: {
        light: '#CD6C4F',
        DEFAULT: '#E37756',
      },
      green: '#27AE60',
      subtext: '#898989',
      secondary: '#E9EBE5',
      background: '#F9F9F9',
      error: '#FB4E4E',
      text: '#373F41',
      gray: {
        1: '#737B7D',
        2: '#C3CBCD',
        3: '#E2E5E6',
        4: '#F4F5F4',
      },
      white: '#FFFFFF',
      accent: {
        light: '#5A82CF',
        DEFAULT: '#3C64B1',
        dark: '#1E4693',
        disabled: '',
      },
      plans: '#E5E5E5',
      'plans-heading': '#2E2E2E',
      disabled: '#F4F5F4',
    },
    placeHolderColor: {
      primary: '#737B7D',
    },
    letterSpacing: {
      textField: '0.2px',
    },
    fontFamily: {
      sans: ['Mulish', 'sans-serif'],
    },
    container: {
      center: true,
    },
    extend: {
      fontWeight: {
        extrabold: 800,
        bold: 700,
        semibold: 600,
        regular: 400,
      },
      fontSize: {
        price: [
          '48px',
          {
            letterSpacing: '0.2px',
            lineHeight: '60px',
          },
        ],
        caption: [
          '14px',
          {
            letterSpacing: '0.4px',
            lineHeight: '18px',
          },
        ],
        button: [
          '14px',
          {
            letterSpacing: '0.3px',
            lineHeight: '18px',
          },
        ],
        small: [
          '12px',
          {
            letterSpacing: '0.2px',
            lineHeight: '16px',
          },
        ],
        'body-2': [
          '14px',
          {
            letterSpacing: '0.2px',
            lineHeight: '20px',
          },
        ],
        'body-1': [
          '16px',
          {
            letterSpacing: '0.3px',
            lineHeight: '22px',
          },
        ],
        'subtitle-2': [
          '14px',
          {
            letterSpacing: '0.2px',
            lineHeight: '18px',
          },
        ],
        'subtitle-1': [
          '16px',
          {
            letterSpacing: '0.1px',
            lineHeight: '22px',
          },
        ],
        h6: [
          '18px',
          {
            letterSpacing: '0.1px',
            lineHeight: '24px',
          },
        ],
        h5: [
          '20px',
          {
            letterSpacing: '0.1px',
            lineHeight: '26px',
          },
        ],
        h4: [
          '26px',
          {
            letterSpacing: '0.2px',
            lineHeight: '32px',
          },
        ],
        h3: [
          '32px',
          {
            letterSpacing: '0.1px',
            lineHeight: '40px',
          },
        ],
        h2: [
          '44px',
          {
            letterSpacing: '0.2px',
            lineHeight: '56px',
          },
        ],
        h1: [
          '56px',
          {
            letterSpacing: '0.2px',
            lineHeight: '72px',
          },
        ],
      },
      backgroundImage: theme => ({
        hero: "url('./assets/garage.png')",
      }),
      borderRadius: {
        DEFAULT: '3px',
      },
      height: {
        hero: '95%',
      },
      padding: {
        full: '100%',
      },
      width: {
        '2/7': '28.5%',
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['active', 'disabled', 'hover'],
      borderColor: ['active', 'disabled', 'focus'],
      borderRadius: ['hover'],
      textColor: ['active', 'disabled'],
      opacity: ['disabled', 'active', 'hover'],
      borderWidth: ['focus, hover', 'disabled'],
      placeholderColor: ['disabled'],
      backgroundOpacity: ['active'],
      cursor: ['disabled'],
    },
  },
  plugins: [],
};
